
/**
 * module dependencies
 */

var errors = require('./lib/errors');
var SMSClient = require('./lib/clients/sms');
var EmailClient = require('./lib/clients/email');
var UserClient = require('./lib/clients/user');
//var ModelPG = require('./models_pg');
//var Category = require('./utils/category');
var ModelID = require('./models_id');
// var ModelActivity = require('./models_activity');
var Util = require('./utils/Util');
/**
 * module exports
 */

module.exports.Clients = {
  SMS: SMSClient,
  Email: EmailClient,
  User: UserClient
};

//exports.ModelPG = ModelPG;
//exports.Category = Category;
module.exports.ModelID = ModelID;
//module.exports.ModelActivity = ModelActivity; // it is because activity mongodb is different than id mongodb, we may merge in the future
module.exports.errors = errors;
module.exports.Util = Util;
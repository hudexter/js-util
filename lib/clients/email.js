
/**
 * module dependencies
 */

var util = require('util');
var request = require('request');
var Promise = require('native-or-bluebird');

var errors = require('../errors');
var Base = require('./client');

/**
 * constants
 */

var required = ['to', 'subject'];

/*
 * module exports
 */

var Client = module.exports = exports = function Client() {
  Base.apply(this, arguments);
}

util.inherits(Client, Base)

Client.prototype.send = function(message) {
  if (!this.validate(required, message)) {
    return Promise.reject(new Error('invalid'));
  }

  var opts = {
    url: util.format('%s/email', this.config.url),
    body: message,
    auth: {
      bearer: this.config.authToken,
    },
    json: true
  };

  return new Promise(function(resolve, reject) {
    return request.post(opts, function(err, resp, body) {
      if (err) {
        return reject(err);
      }

      if (resp.statusCode >= 200 && resp.statusCode <= 299) {
        return resolve(body);
      } else {
        reject(new errors.BaseError(resp.statusCode, 'send.error', 'invalid http response code'));
        return;
      }
    });
  });
};

exports.required = required;

/**
 * module dependencies
 */

var util = require('util');
var Promise = require('native-or-bluebird');
var request = require('request');

var errors = require('../errors');
var Base = require('./client');

/**
 * constants
 */
var required = [];

/**
 *  module exports
 */

var ActivityClient = module.exports = exports = function ActivityClient() {
  Base.apply(this, arguments);
}

util.inherits(ActivityClient, Base);

ActivityClient.prototype.send = function(data) {
  if (!this.validate(required, data)) {
    return Promise.reject(new Error('invalid'));
  }

  var opts = {
    url: util.format('%s/notifications', this.config.url),
    auth: {
      bearer: this.config.authToken,
    },
    body: data,
    json: true
  };

  return new Promise(function(resolve, reject) {
    return request.post(opts, function(err, resp, body) {
      if (err) {
        reject(err);
        return;
      }

      if (resp.statusCode >= 200 && resp.statusCode <= 299) {
        resolve();
        return;
      }

      return reject(new errors.BaseError(resp.statusCode, 'send.error', 'invalid http resp code'));
    });
  });
};

exports.required = required;

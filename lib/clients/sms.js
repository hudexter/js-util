
/**
 * module dependencies
 */

var util = require('util');
var Promise = require('native-or-bluebird');
var request = require('request');

var errors = require('../errors');

var Base = require('./client');

/**
 * constants
 */

var required = ['to', 'body'];

/**
 * module exports
 */

var Client = module.exports = exports = function() {
  Base.apply(this, arguments);
};

util.inherits(Client, Base);

Client.prototype.send = function(data) {
  if (!this.validate(required, data)) {
    return Promise.reject(new Error('Incorrect parameters'));
  }

  var endpoint = util.format('%s/sms', this.config.url);

  var opts = {
    url: endpoint,
    body: data,
    auth: {
      bearer: this.config.authToken,
    },
    json: true
  };

  return new Promise(function(resolve, reject) {
    return request.post(opts, function(err, resp, body) {
      if (err) {
        return reject(err);
      }

      if (resp.statusCode >= 200 && resp.statusCode <= 299) {
        return resolve(body);
      } else {
        reject(new errors.BaseError(resp.statusCode,'send.error', 'invalid http response code'));
        return;
      }
    });
  });
};

exports.required = required;

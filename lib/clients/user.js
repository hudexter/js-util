
/**
 * module dependencies
 */

var util = require('util');
var Promise = require('native-or-bluebird');
var request = require('request');

var errors = require('../errors');

var Base = require('./client');

/**
 * constants
 */

var required = ['to', 'body'];

/**
 * module exports
 */

var Client = module.exports = exports = function() {
  Base.apply(this, arguments);
};

util.inherits(Client, Base);

Client.prototype.get = function() {

  var endpoint = util.format('%s/api/profile?id=%s', this.config.url, this.config.uid);

  var opts = {
    url: endpoint,
    auth: {
      bearer: this.config.authToken,
    }
  };

  return new Promise(function(resolve, reject) {
    return request.get(opts, function(err, resp, body) {
      if (err) {
        return reject(err);
      }

      if (resp.statusCode >= 200 && resp.statusCode <= 299) {
        return resolve(body);
      } else {
        reject(new errors.BaseError(resp.statusCode, 'send.error', 'invalid http response code'));
        return;
      }
    });
  });
};

exports.required = required;
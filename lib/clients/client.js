
/**
 * module dependencies
 */

var Promise = require('native-or-bluebird');

/**
 * module.exports
 */

var Base = function Base(config) {
  if (!config.url || !config.authToken) {
    throw new Error('missing url or authToken');
  }

  this.config = config;
}

/**
 * send function that processes http request
 * @return {null}
 */

Base.prototype.send = function() {
  return Promise.reject(new Error('method `send` not yet implemented!'));
};

/**
 * validate validates if data contains required fields.
 * @param  {Object} data Object to validate against
 * @return {boolean}      Object passes validation?
 */
Base.prototype.validate = function(required, data) {
  if (!required || !Array.isArray(required)) {
    throw new Error('missing required fields');
  }

  if (typeof data !== 'object') {
    throw new Error('data is not an object');
  }

  return required.every(function(field) {
    return !!data[field];
  });
};

module.exports = Base;

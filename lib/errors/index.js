
/**
 * module dependencies
 */

var util = require('util');
var Promise = require('native-or-bluebird');

/**
 * module exports
 */
var errors = module.exports = {};

var AntError = errors.BaseError = function AntError(code, type, message, error) {
  Error.call(this);
  Error.captureStackTrace(this, Error);

  this.code = code;
  this.message = message;
  this.type = type;
  this.inner = error || {};
}
util.inherits(AntError, Error);

AntError.prototype.shim = function() {
  return Promise.reject(this);
};

var NotFoundError = errors.NotFound = function NotFoundError(message) {
  AntError.call(this, 404, message, 'not_found');
};
util.inherits(NotFoundError, AntError);

errors.transform = function(err) {
  err = err instanceof Error ? err : new Error(err);

  return new AntError(500, 'generic', err.message, err);
};

var jwt = require('jsonwebtoken');
var unless = require('express-unless');
var async = require('async');

var DEFAULT_REVOKED_FUNCTION = function(_, __, cb) {
  return cb(null, false);
}

var getClass = {}.toString;

function isFunction(object) {
  return object && getClass.call(object) == '[object Function]';
}

function wrapStaticSecretInCallback(secret) {
  return function(_, __, cb) {
    return cb(null, secret);
  };
}

module.exports = function(req, options) {

  var isRevokedCallback = options.isRevoked || DEFAULT_REVOKED_FUNCTION;

  var _requestProperty = options.userProperty || options.requestProperty || 'user';
  var credentialsRequired = typeof options.credentialsRequired === 'undefined' ? true : options.credentialsRequired;


  if (req.headers && req.headers.authorization) {
    var parts = req.headers.authorization.split(' ');
    if (parts.length == 2) {
      var scheme = parts[0];
      var credentials = parts[1];

      if (/^Bearer$/i.test(scheme)) {
        token = credentials;
      }
    }
  }

  if (!token) {
    return req;
  }

  var dtoken = jwt.decode(token, {
    complete: true
  }) || {};

  async.parallel([
    function(callback) {
      var arity = secretCallback.length;
      if (arity == 4) {
        secretCallback(req, dtoken.header, dtoken.payload, callback);
      } else { // arity == 3
        secretCallback(req, dtoken.payload, callback);
      }
    },
    function(callback) {
      isRevokedCallback(req, dtoken.payload, callback);
    }
  ], function(err, results) {
    if (err) {
      console.log(err);
    }

    var revoked = results[1];
    if (revoked) {
      console.log({
        message: 'The token has been revoked.'
      });
    }

    var secret = results[0];

    jwt.verify(token, secret, options, function(err, decoded) {
      if (err && credentialsRequired)
        console.log('invalid_token');
      console.log(decoded);
      req[_requestProperty] = decoded;

    });
  });

  return req;
}
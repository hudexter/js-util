
var mongoose = require('mongoose');

module.exports = function(conn, mongoose) {
  var mediaLinkSchema = new mongoose.Schema({
    duration: {
      type: Number
    },
    height: {
      type: Number
    },
    width: {
      type: Number
    },
    url: {
      type: String
    }
  });

  var medialink = conn.model('MediaLink', mediaLinkSchema, 'media_ink');
  return medialink;

};
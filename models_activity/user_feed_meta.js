'use strict';

var mongoose = require('mongoose');



module.exports = function(conn, mongoose) {

  var userFeedMetaSchema = new mongoose.Schema({
    id: {
      type: Number,
      required: true,
    },
    last_query_timestamp: {
      type: Date
    },
    last_socialfeed_query_timestamp: {
      type: Date
    }
    
  });

  userFeedMetaSchema.index({ id: 1 });

  return conn.model('UserFeedMeta', userFeedMetaSchema);
}

//module.exports = mongoose.model('userFeedMeta', userFeedMetaSchema);
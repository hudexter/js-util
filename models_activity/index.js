
/**
 * Generated file from sequelize-cli
 */
var fs = require("fs");
var path = require("path");
var config = require("as-config");
var Promise = require("bluebird");

var mongoose = require('mongoose');
var conn1 = mongoose.createConnection(config.activity_mongdb_url || process.env.ACTIVITY_MONGODB_URL, { server: { ssl: true }});

Promise.promisifyAll(mongoose);

var db = {};

fs
  .readdirSync(__dirname)
  .filter(function(file) {
    return (file.indexOf(".") !== 0) && (file !== "index.js");
  })
  .forEach(function(file) {
    //console.log(path.join(__dirname, file));
    var model = require(path.join(__dirname, file))(conn1, mongoose);
    db[model.modelName] = model;
  });

db.mongoose = mongoose;
db.connection = conn1;
module.exports = db;
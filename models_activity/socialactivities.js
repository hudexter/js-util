
var config = require("as-config");
var mongoose = require('mongoose');
var autoIncrement = require('mongoose-auto-increment');
var idGen = require('../utils/ext-id-generator');
var mongoosePaginate = require('mongoose-paginate');
var _ = require('lodash');
var algoliasearch = require('algoliasearch');
var algo_client = algoliasearch(config.algolia.application_id, process.env.ALGOLIA_KEY || config.algolia.api_key);
var social_card_index = algo_client.initIndex(config.algolia.social_card_index);
social_card_index.setSettings({
  attributesToIndex: ['id', 'activity_id', 'published_timestamp', 'is_active', 'reason', 'receiver_id', 'activity'],
  customRanking: ['desc(published_timestamp)'],
  attributesForFaceting: ['is_active', 'reason']
});
var models = require('as-util').ModelPG;
var logger = require('as-client').logger(config.slack, config.url);

module.exports = function(conn, mongoose) {


  autoIncrement.initialize(conn);

  var deepPopulate = require('mongoose-deep-populate')(mongoose);

  var socialactivitySchema = new mongoose.Schema({
    id: {
      type: Number,
      index: true
    },
    actor_id: {
      type: Number
    },
    target_id: {
      type: Number
    },
    activity_id: {
      type: Number
    },
    verb: {
      type: String,
      enum: ['new_product', 'disapprove_product', 'like_product', 'comment_product',
        'new_service', 'like_service', 'comment_service',
        'new_order', 'cancel_order', 'new_chat', 'new_following', 'store_review',
        'buy_product', 'buy_service', 'new_moment', 'like_moment', 'comment_moment',
        'recommended_users', 'recommended_stores', 'sponsored_product', 'sponsored_service', 'sponsored_moment', 'system_announcement', 'system_reminder', 'new_user_mention', 'new_store_mention', 'store_announcement', 'system_notification_announcement', 'system_notification_upgrade', 'system_notification_add_creditcard', 'system_notification_marketing_campaign', 'system_notification_change_picture', 'system_notification_claim_username', 'system_notification_claim_storehandle', 'share_link', 'new_request', 'like_request', 'comment_request', 'new_newsevent'
      ],
      required: false
    },
    is_active: {
      type: Boolean,
      default: true
    },
    reason: {
      type: String
    },
    receiver_id: {
      type: Number
    },
    activity: { type: mongoose.Schema.Types.ObjectId, ref: 'Activities' },
    activity_json: {},
    published: {
      type: Date,
      default: Date.now
    },

    published_timestamp: {
      type: Number,
      default: new Date().getTime()
    },

  });

  socialactivitySchema.index({
    "verb": 1
  });
  socialactivitySchema.index({
    "activity_id": 1
  });
  socialactivitySchema.index({
    "receiver_id": 1
  });
  socialactivitySchema.index({
    "published_timestamp": 1
  });


  /* plugin */
  socialactivitySchema.plugin(autoIncrement.plugin, {
    model: 'socialactivities',
    field: 'id',
    startAt: 10000,
    incrementBy: 1
  });

  socialactivitySchema.plugin(mongoosePaginate);

  socialactivitySchema.plugin(deepPopulate);

  // socialactivitySchema.pre('save', function(next) {
  //   console.log("soical activity pre save handler")
  //   if (!this.published) {
  //     this.published = new Date();
  //     this.published_timestamp = this.published.getTime();
  //   }

  //   next();
  // });

  // socialactivitySchema.post('save', function(doc) {
  //   console.log("social activity post save handler");
  //   social_card_index.saveObject(_.merge({
  //     objectID: doc.id
  //   }, doc), function(err, content) {
  //     //updateStoreProductCount(storeRecord.store_id)
  //     if (err) {
  //       logger.error(err);
  //       console.log("store_product_index save fail: " + err);
  //     } else {
  //       console.log("done cards index update")
  //     }
  //   })

  // });

  socialactivitySchema.methods.toAlgoliaJSON = function() {
    //return this.deepPopulate('activity').execPopulate();

    return this;
  }

  // var convertExtIdToId = function(xid) {
  //   var tempXid = xid || this.xid || '';
  //   return parseInt(idGen.decryptId({
  //     salt: config.env().salt.activities,
  //     message: tempXid
  //   }), 10);
  // };

  return conn.model('Socialactivities', socialactivitySchema);
}
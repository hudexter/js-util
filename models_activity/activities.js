'use strict';

var config = require("as-config");
var mongoose = require('mongoose');
var autoIncrement = require('mongoose-auto-increment');
var idGen = require('../utils/ext-id-generator');
var mongoosePaginate = require('mongoose-paginate');
var _ = require('lodash');
var algoliasearch = require('algoliasearch');
var algo_client = algoliasearch(config.algolia.application_id, process.env.ALGOLIA_KEY || config.algolia.api_key);
var activity_index = algo_client.initIndex(config.algolia.activity_index);
activity_index.setSettings({
  attributesToIndex: ['aid', 'xid', 'activity_type', 'actor', 'verb', 'product', 'request', 'announcement', 'review', 'store', 'message', 'payment', 'published', 'published_timestamp', 'target', '_geoloc', 'category', 'categoryId', 'ref', '_tags', 'category_info', 'followers', 'loc', 'location'],
  customRanking: ['desc(published_timestamp)'],
  attributesForFaceting: ['category', 'type', 'categoryId']
});

//var SocialActivity = require('./index.js').Socialactivities;
var logger = require('as-client').logger(config.slack, config.url);

// var models = require('../models_pg');
// var SocialActivity = require('./socialfeed');
// var User = require('../models_id/index.js').User;


var maxDistance = config.nearby_distance / 6371;
module.exports = function(conn, mongoose) {


  autoIncrement.initialize(conn);

  var activitySchema = new mongoose.Schema({
    xid: {
      type: String
    },
    activity_type: [String], // "following" or "self"
    //self, target, following, nearby, all
    actor: {
      id: {
        type: Number,
        required: false
      },
      xid: {
        type: String,
        required: false
      },
      name: {
        type: String
      },
      picture: {
        type: String
      },
      total_followers: {
        type: Number
      },
      store_info: {},
      as_business: Boolean
    },

    verb: {
      type: String,
      enum: ['new_product', 'disapprove_product', 'like_product', 'comment_product',
        'new_service', 'like_service', 'comment_service',
        'new_order', 'cancel_order', 'new_chat', 'new_following', 'store_review',
        'buy_product', 'buy_service', 'new_moment', 'like_moment', 'comment_moment',
        'recommended_users', 'recommended_stores', 'sponsored_product', 'sponsored_service', 'sponsored_moment', 'system_announcement', 'system_reminder', 'new_user_mention', 'new_store_mention', 'store_announcement', 'system_notification_announcement', 'system_notification_upgrade', 'system_notification_add_creditcard', 'system_notification_marketing_campaign', 'system_notification_change_picture', 'system_notification_claim_username', 'system_notification_claim_storehandle', 'share_link', 'new_request', 'like_request', 'comment_request', 'new_newsevent'
      ],
      required: false
    },

    target: {
      id: {
        type: Number,
        required: false
      },
      xid: {
        type: String,
        required: false
      },
      name: {
        type: String
      },
      picture: {
        type: String
      },
      total_followers: {
        type: Number
      },
      store_info: {},
      as_business: Boolean
    },

    product: mongoose.Schema.Types.Mixed,
    service: mongoose.Schema.Types.Mixed,
    request: mongoose.Schema.Types.Mixed,
    moment: mongoose.Schema.Types.Mixed,
    review: mongoose.Schema.Types.Mixed,
    comment: mongoose.Schema.Types.Mixed,
    chat: mongoose.Schema.Types.Mixed,
    store: mongoose.Schema.Types.Mixed,
    newsevent: mongoose.Schema.Types.Mixed,
    ui_message: {
      type: String
    },
    payment: {
      price_in_cents: Number,
      price_type: String,
    },
    reminder: {
      description: String
    },
    weblinksummary: {},
    recommended_users: [],
    recommended_stores: [],

    // target_json: {
    //   id: Number,
    //   user_id: Number,
    //   store_id: Number,
    //   resource_id: String,
    //   type: String,
    //   name: String,
    //   description: String,
    //   content: String,
    //   star: Number,
    //   price_in_cents: Number,
    //   price_type: {
    //     type: String,
    //     enum: ['normal', 'from', 'perhour', 'noprice'],
    //     default: 'normal'
    //   },
    //   location: {
    //     lat: Number,
    //     lon: Number
    //   },
    //   distance_in_km: String,
    //   distance_in_miles: String,
    //   images: [],
    //   video_thumbnail: String,
    //   video_url: String,
    //   tags: String,
    //   first5likers: [],
    //   total_likes: Number,
    //   total_comments: Number,
    //   last5comments: [],
    //   is_liked: {
    //     type: Boolean,
    //     default: false
    //   },
    //   is_following: {
    //     type: Boolean,
    //     default: false
    //   },
    //   created_at: {
    //     type: Date,
    //     default: Date.now
    //   },
    //   unix_time: {
    //     type: Number,
    //     default: new Date().getTime()
    //   },
    //   users: [],
    //   stores: [],
    //   user_info: {},
    //   store_info: {},
    //   url: String,
    //   domain: String
    // },

    target_json: {},


    next_screen: {
      resource_type: {
        type: String,
        enum: ['product', 'service', 'app_page', 'moment',
          'url', 'unknown', 'user', 'store'
        ],
        default: 'unknown'
      },
      key: String
    },
    card_type: {
      type: String,
      enum: ['product', 'service', 'system_notification', 'moment',
        'recommended_users', 'recommended_stores', 'store_review', 'unknown', 'newsevent'
      ],
      default: 'unknown',
      required: false
    },
    card_size: {
      type: String,
      default: 'big',
      enum: ['big', 'small'],
      required: false
    },
    followings: [],
    is_active: {
      type: Boolean,
      default: true
    },
    loc: {
      type: [Number], // [<longitude>, <latitude>]
      index: '2d' // create the geospatial index
    },
    published: {
      type: Date,
      default: Date.now
    },

    published_timestamp: {
      type: Number,
      default: new Date().getTime()
    }
  });

  activitySchema.index({ loc: 1 });
  /* Indexes */
  activitySchema.index({
    "aid": -1
  });
  activitySchema.index({
    "xid": 1
  });
  activitySchema.index({
    "activity_type": 1
  });
  activitySchema.index({
    "verb": 1
  });
  activitySchema.index({
    "published": 1
  });
  activitySchema.index({
    "is_active": 1
  });
  activitySchema.index({
    "target.id": 1,
    "activity_type": 1
  });
  activitySchema.index({
    "followings": 1
  });
  activitySchema.index({
    "product.id": -1
  });
  activitySchema.index({
    "moment.id": -1
  });
  activitySchema.index({
    "product.xid": 1
  });
  activitySchema.index({
    "published_timestamp": -1
  });

  activitySchema.methods.toElasticSearchModel = function() {

    var result = {};
    result = this;

    if (result.loc && result.loc.length == 2) {
      result._geoloc = {
        lat: Number(result.loc[1]),
        lng: Number(result.loc[0])
      }
      result.location = {
        lat: result.loc[1],
        lon: result.loc[0]
      }
    }
    return result;

  };



  /* plugin */
  activitySchema.plugin(autoIncrement.plugin, {
    model: 'activities',
    field: 'aid',
    startAt: 1000,
    incrementBy: 1
  });

  activitySchema.plugin(mongoosePaginate);



  activitySchema.pre('save', function(next, done) {
    console.log('locking 2');
    // this.xid = idGen.encryptId({
    //   salt: config.salt.activities,
    //   id: this.aid,
    //   prefix: "a",
    //   autoPostfix: true
    // });

    if (!this.published) {
      this.published = new Date();
      this.published_timestamp = this.published.getTime();
    }

    // if (_.isEmpty(this.target_json)) {

    //   Util.getFeedModel({
    //     product_id: _.get(this.product, 'id', 0),
    //     moment_id: _.get(this.moment, 'id', 0),
    //     newsevent_id: _.get(this.newsevent, 'id', 0)
    //   }).then(function(data) {
    //     this.target_json = data;
    //     process.nextTick(function() {
    //       console.log('releasing lock 2');
    //       //done();
    //     });
    //     next();
    //   })

    // } else {
    process.nextTick(function() {
      console.log('releasing lock 2');
      //done();
    });
    next();
    //}

    // if (this.is_active != doc.is_active) {
    //   socialactivities.findAsync({ activity_id: this.aid }).then(function(scs) {
    //     var all = _.map(scs, function(sc) {
    //       sc.is_active = doc.is_active;
    //       return sc.save();
    //     })
    //     return Promise.all(all);
    //   }).then(function(scs) {
    //     console.log('updated related social activities to ' + doc.is_active);
    //   });
    // }
    //process.nextTick(next);

  });

  activitySchema.post('save', function(doc) {
    console.log("activity post save handler")
      //doc = doc.toObject();
      // doc.ownerID = doc.actor.id
      // doc.unix_time = Date.now();
      // doc.type = doc.verb

    //target user can be contained in followers array.
    //here we use object keys as a 'set structure' 
    //to prevent the same card from getting pushed to a user twice
    var notified_users = {}
    _.map(doc.followings, function(follower) {
      notified_users[follower] = true
    })
    if (doc.target) {
      notified_users[doc.target.xid] = true
    }
    // if (doc.actor) {
    //   notified_users[doc.actor.xid] = true
    // }


    // var records = [];
    // doc.notification = 0;
    // doc.social = 0;
    // records.push({
    //   action: 'addObject',
    //   objectID: 'feed' + doc.aid + "_me",
    //   indexName: config.env().algolia.cards_index,
    //   body: _.merge({
    //     is_visible: 1,
    //     approved: 1,
    //     is_enabled: 1,
    //     is_deleted: 0,
    //     actorId: doc.actor.id
    //   }, doc)
    // })

    //injection for nearby user

    // User.find({
    //   loc: {
    //     $near: doc.loc,
    //     $maxDistance: maxDistance
    //   }
    // }).limit(100).exec(function(err, users) {

    //   console.log('nearby user # to doc.doc');
    //   console.log(users.length);
    //   _.map(users, function(user) {
    //     notified_users[user.uid] = true
    //   });

    // _.map(Object.keys(notified_users), function(follower) {
    //   delete doc.ownerID
    //   if (doc.type == "new_product" || doc.type == "new_request" || doc.type == "store_announcement") {
    //     doc.notification = 0;
    //     doc.social = 1;
    //   } else if (doc.type == "comment_product" || doc.type == "new_following" || doc.type == "like_product") {
    //     doc.notification = 1;
    //     doc.social = 0;

    //   }

    //   records.push({
    //     action: 'addObject',
    //     objectID: 'feed' + doc.aid + "_" + follower,
    //     indexName: config.env().algolia.cards_index,
    //     body: _.merge({
    //       ownerID: Number(follower),
    //       is_visible: 1,
    //       approved: 1,
    //       is_enabled: 1,
    //       is_deleted: 0,
    //       actorId: doc.actor.id
    //     }, doc)
    //   })

    //   SocialActivity.create({
    //     activity_id: doc.aid,
    //     is_active: true,
    //     reason: 'following',
    //     receiver_id: follower,
    //     activity: doc
    //   });
    // })
    //});

    //console.log("batch records: " + JSON.stringify(records));
    // algo_client.batch(records, function(err, content) {
    //   if (err) {
    //     console.trace();
    //     console.error(err);
    //   }
    // });
    //}


    // if (activity.is_active != sc.is_active) {
    //   Activities.findOneAsync({ "_id": new ObjectId(sc.activity) }).then(function(activity1) {
    //     activity1.is_active = activity.is_active;
    //     sc.is_active = activity.is_active;
    //     console.log(activity.is_active);
    //     console.log('update activity and socialactivity is_active from model');
    //     console.log(activity1);
    //     console.log(sc);
    //     activity1.saveAsync();
    //     sc.saveAsync();
    //   });
    // }

    // if (doc.is_active == false) {
    //   SocialActivity.update({ activity_id: doc.aid }, { is_active: false }, { multi: true }, function(err, num) {
    //     if (err) {
    //       console.log(err.stack);
    //     } else {
    //       console.log('updated related socialactivities: ' + num);
    //     }
    //   })
    // }

    // activity_index.saveObject(_.merge({
    //   objectID: doc.aid
    // }, doc), function(err, content) {
    //   //updateStoreProductCount(storeRecord.store_id)
    //   if (err) {
    //     // logger.error(err);
    //     console.log("store_product_index save fail: " + err);
    //   } else {
    //     console.log("done cards index update")
    //   }
    // });

  });

  var convertExtIdToId = function(xid) {
    var tempXid = xid || this.xid || '';
    return parseInt(idGen.decryptId({
      salt: config.salt.activities,
      message: tempXid
    }), 10);
  };

  //var SocialActivity = socialactivities || require('./socialactivities')(conn, mongoose);

  return conn.model('Activities', activitySchema);
}
'use strict';

var util = require('util');
var request = require('request');
var sinon = require('sinon');
var chai = require('chai');

var expect = chai.expect;

chai.config.includeStack = true;

describe('Activity Client', function() {
  var Client = require('../../lib/clients/activity');

  var config;

  beforeEach(function() {
    config = {
      url: 'http://fake.com',
      authToken: 'abcdefg'
    };
  });

  describe('.Generic', function() {
    it('should be able to have a client', function() {
      expect(Client).to.be.an('function');
    });

    it('should export required fields', function() {
      expect(Client).to.have.property('required').to.be.an('array');
    });

    it('should catch error when authToken||url not found', function() {
      function f(x) {
        var c = new Client(x);
        return c;
      }

      var x = null;

      x = {
        url: 'http://a.com'
      };

      expect(x).to.have.property('url');
      expect(x).to.not.have.property('authToken');

      expect(f.bind(null, x)).to.throw(Error);

      x = {
        authToken: 'abddd'
      };

      expect(x).to.have.property('authToken');
      expect(x).to.not.have.property('url');

      expect(f.bind(null, x)).to.throw(Error);
    });

    it('should get client when required fields are present', function() {
      function f() {
        var c = new Client(config);
        return c;
      }

      expect(f).to.not.throw(Error);

      var c = f();
      expect(c).to.be.an.instanceof(Client);
    });
  });

  describe('.Send', function() {
    it('should be able to catch error when statusCode != 2xx', function(done) {
      sinon.stub(request, 'post').yields(null, { statusCode: 300 });

      var data = {
        a: 'b'
      };

      var c = new Client(config);
      c.send(data)
        .catch(function(err) {
          expect(err).to.exist;

          expect(err).to.have.property('code').to.be.equal(300);

          request.post.restore();

          done();
        });
    });

    it('should be able to catch error when request yielding error', function(done) {
      sinon.stub(request, 'post').yields(new Error('fake'));

      var data = {
        a: 'b'
      };

      var c = new Client(config);
      c.send(data)
        .catch(function(err) {
          expect(err).to.exist;

          request.post.restore();
          done();
        });
    });

    it('should be able to send data', function(done) {
      sinon.stub(request, 'post').yields(null, { statusCode: 201 });

      var data = {
        a: 'b'
      };

      var c = new Client(config);
      c.send(data)
        .then(function() {
          expect(request.post.calledWith({
            body: data,
            json: true,
            url: util.format('%s/notifications', config.url),
            auth: {
              bearer: config.authToken
            }
          })).to.be.true;

          request.post.restore();

          done();
        });
    });
  });
});

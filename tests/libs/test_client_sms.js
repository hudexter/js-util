'use strict';

var request = require('request');
var util = require('util');

var chai = require('chai');
var sinon = require('sinon');

var assert = chai.assert;
var expect = chai.expect;

chai.config.includeStack = true;

describe('SMS Client', function() {
  var Client = require('../../lib/clients/sms');

  var config;

  beforeEach(function() {
    config = {
      url: 'http://fake.com',
      authToken: 'fake-long-token'
    };
  });

  describe('.Generic', function() {
    it('should have a construtor', function() {
      expect(Client).to.be.an('function');
    });

    it('should have the required fields', function() {
      expect(Client.required).to.be.an('array');
    });
  });

  describe('.Send', function() {
    it('should throw error when url/authToken is missing', function() {
      delete config.authToken;

      function t() {
        var c = new Client(config);
        return c;
      }

      expect(t).to.throw(Error);
    });

    it('should catch error when the required fields do not exist', function(done) {
      sinon.stub(request, 'post');

      var data = {
        a: 'b'
      };

      Object.keys(Client.required).forEach(function(field) {
        expect(data).to.not.have.property(field);
      });

      var client = new Client(config);

      client.send(data).catch(function(err) {
        expect(err).to.be.an.instanceof(Error);

        assert.ok(request.post.notCalled);
        request.post.restore();

        done();
      });
    });

    it('should be able to send sms', function(done) {
      sinon.stub(request, 'post').yields(null, { statusCode: 201 });

      var client = new Client(config);

      var data = {
        to: 6041112222,
        body: 'Hello world'
      };

      client
        .send(data)
        .then(function() {
          assert.ok(request.post.calledWith({
            url: util.format('%s/sms', config.url),
            body: {
              to: 6041112222,
              body: 'Hello world'
            },
            auth: {
              bearer: config.authToken
            },
            json: true
          }));

          assert.ok(request.post.calledOnce);
          request.post.restore();

          done();
        });
    });

    it('should catch err when send sms fails', function(done) {
      sinon.stub(request, 'post').yields(null, { statusCode: 400 });

      var client = new Client(config);

      var data = {
        to: 6041112222,
        body: 'hello~'
      };

      client
        .send(data)
        .catch(function(err) {
          expect(err).to.have.property('code').to.be.equal(400);

          request.post.restore();
          done();
        });
    });
  });
});

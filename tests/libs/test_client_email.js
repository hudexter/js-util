'use strict';

var util = require('util');
var request = require('request');
var sinon = require('sinon');
var chai = require('chai');

var expect = chai.expect;
var assert = chai.assert;

chai.config.includeStack = true;

describe('Email Client', function() {
  var Client = require('../../lib/clients/email');

  var config;

  beforeEach(function() {
    config = {
      url: 'http://fake.com',
      authToken: 'fake-long-token'
    };
  });

  describe('.Generic', function() {
    it('should be a constructor', function() {
      expect(Client).to.be.a('function');
    });

    it('should have required fields', function() {
      expect(Client).to.have.property('required').to.be.an('array');
    });
  });

  describe('.Send', function() {
    it('should throw error when url/authToken is missing', function() {
      delete config.authToken;

      function t() {
        var c = new Client(config);
        return c;
      }

      expect(t).to.throw(Error);
    });

    it('should be able to catch error on invalid', function(done) {
      sinon.stub(request, 'post').yields(null, { statusCode: 200 });

      var client = new Client(config);
      var message = {};

      return client
        .send(message)
        .catch(function(err) {
          expect(err).to.exist;

          assert.ok(request.post.notCalled);

          request.post.restore();
          done();
        });
    });

    it('should be able to send message', function(done) {
      sinon.stub(request, 'post').yields(null, { statusCode: 201 });

      var client = new Client(config);
      var message = {
        to: 'test@antsquare.com',
        body: '<b>Hello</>'
      };

      client
        .send(message)
        .then(function() {
          assert.ok(request.post.calledWith({
            json: true,
            auth: {
              bearer: config.authToken
            },
            url: util.format('%s/email', config.url),
            body: message
          }));

          assert.ok(request.post.calledOnce);

          request.post.restore();
          done();
        });
    });

    it('should be able to catch when send message fails', function(done) {
      sinon.stub(request, 'post').yields(null, { statusCode: 401 });

      var client = new Client(config);

      var message = {
        to: 'a@ant.square',
        body: 'hello world'
      };

      client
        .send(message)
        .catch(function(err) {
          expect(err).to.have.property('code').to.be.equal(401);

          request.post.restore();

          done();
        });
    });
  });
});

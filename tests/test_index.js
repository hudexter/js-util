'use strict';

var utilities = require('../');

var chai = require('chai');
var expect = chai.expect;

chai.config.includeStack = true;

describe('Index', function() {
  it('should be able to have correct exports', function() {
    expect(utilities).to.have.keys([
      'Clients',
      'errors',
    ]);
  });

  describe('.Clients', function() {
    it('should have correct exports', function() {
      expect(utilities.Clients).to.have.keys([
        'SMS',
        'Email',
      ]);
    });
  });
});

'use strict';

var chai = require('chai');
var expect = chai.expect;

chai.config.includeStack = true;

describe('Error', function() {
  var errors = require('../../lib/errors');

  describe('.Generic', function() {
    it('should be have correct exports', function() {
      expect(errors).to.have.keys([
        'NotFound',
        'BaseError',
        'transform'
      ]);
    });
  });

  describe('.NotFound', function() {
    it('should be able to generate NotFoundError', function(done) {
      var notFound = new errors.NotFound('something gone wrong');

      expect(notFound).to.be.an.instanceof(Error);
      expect(notFound).to.be.an.instanceof(errors.BaseError);
      expect(notFound).to.be.an.instanceof(errors.NotFound);

      notFound.shim().catch(function(err) {
        expect(err).to.have.property('code').to.be.equal(404);

        done();
      });
    });
  });

  describe('.Transform', function() {
    it('should be able to transform a regular error to AntError', function(done) {
      var err = new Error('fake');

      expect(err).to.not.be.an.instanceof(errors.BaseError);

      var antErr = errors.transform(err);

      expect(antErr).to.be.an.instanceof(errors.BaseError);


      antErr.shim().catch(function(err) {
        expect(err).to.have.property('message').to.be.equal('fake');
        expect(err).to.have.property('code').to.be.equal(500);

        done();
      });
    });
  });
});

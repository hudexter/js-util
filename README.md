antsquare utilities (as-util)
=======

This module should hold all common utils for various services.

## Clients (Email/SMS)

All services client implements `send` method to maintain consistency, and follows Promise style.

```js
var utils = require('as-util');

var config = {
  url: 'path_to_sms_service',
  authToken: '<authorization token/uuid>'
};

var smsClient = utils.Clients.SMS(config);

return smsClient
        .send(message)
        .then(function() {
          // succeeded
        })
        .catch(function(err) {
          // shoot error occurred
        });
```

## Errors

To generate a form of AntError you can use the Error utility.

```js
var errors = require('as-util').errors;

var notFoundError = errors.NotFound('Something gone wrong');

if (notFoundError instanceof errors.notFound) {
  // this will be true!
}

// You can also `promisify` this. The following will return a Promise.reject();
function testable() {
  // some error happened.
  if (notFoundError)
    return notFoundError.shim();

  return Promise.resolve();
}

// You can also convert a regular error to antError/BaseError

var err = new Error('regular');

var newErr = errors.transform(err);

if (newError instanceof errors.BaseError) {
  console.log('this will be true!');
}

```

## Tests

To run tests: `npm test`



exports.env = function() {

  var env = process.env.NODE_ENV;

  if (env === 'production') {
    return require('./config/production');
  } else if (env === 'staging') {
    return require('./config/staging');
  } else if (env === 'test') {
    return require('./config/test');
  } else {
    return require('./config/development');
  }


};
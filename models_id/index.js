"use strict";

var fs = require("fs");
var path = require("path");
var config = require("as-config");
var Promise = require("bluebird");
//var autoIncrement = require('mongoose-auto-increment');
var mongoose = require('mongoose');
var conn2 = mongoose.createConnection(config.id_mongdb_url || process.env.ID_MONGODB_URL, { server: { ssl: true } });
Promise.promisifyAll(mongoose);

//console.log(config.id_mongdb_url || process.env.ID_MONGODB_URL);
//autoIncrement.initialize(conn);

var db = {};

fs
  .readdirSync(__dirname)
  .filter(function(file) {
    return (file.indexOf(".") !== 0) && (file !== "index.js");
  })
  .forEach(function(file) {
    //console.log(path.join(__dirname, file));
    var model = require(path.join(__dirname, file))(conn2, mongoose);
    db[model.modelName] = model;
  });

db.mongoose = mongoose;
db.connection = conn2;
module.exports = db;
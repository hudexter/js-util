'use strict';

var _ = require('lodash');

//Promise = require('bluebird'),
// mongoose = require('mongoose'),
// Schema = mongoose.Schema;

//Promise.promisifyAll(mongoose);

module.exports = function(conn, mongoose) {

  var deviceSchema = new mongoose.Schema({
    uid: {
      type: Number
    },
    // user: {
    //   type: 'String'
    // },
    type: {
      type: 'String',
      required: true,
      enum: ['ios', 'android'],
      lowercase: true
    },
    token: {
      type: 'String',
      required: true
    },
    status: {
      type: 'String',
      enum: ['online', 'offline'],
      lowercase: true,
      default: 'online'
    },
    app_version: {
      type: 'String'
    },
    os_version: {
      type: 'String'
    },
    location: {
      lat: Number,
      lon: Number
    },
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now }
  });

  // deviceSchema.index({
  //   user: 1,
  //   token: 1
  // }, {
  //   unique: true
  // });

  // deviceSchema.index({
  //   user: 1
  // }, {
  //   unique: true
  // });

  // deviceSchema.index({
  //   token: 1
  // }, {
  //   unique: true
  // });

  var device = conn.model('Device', deviceSchema, 'device');
  return device;
};
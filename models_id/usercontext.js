'use strict';

var _ = require('lodash');

//Promise.promisifyAll(mongoose);


module.exports = function(conn, mongoose) {
  var usercontextSchema = new mongoose.Schema({
    uid: Number,
    loc: {
      type: [Number], // [<longitude>, <latitude>]
      index: '2d' // create the geospatial index
    },
    meetups: [{
      transaction_id: Number,
      is_buyer: Boolean,
      seller_id: Number,
      buyer_id: Number,
      deliver_id: Number,
      date: Date,
      location: {
        lat: Number,
        lon: Number
      },
      status: String
    }],
    location: {
      lat: Number,
      lon: Number
    },
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now  }
  });



  /**
   * @Constructor
   * @param {String} id : The id of the new user
   * @returns {Object} : The user
   **/
  usercontextSchema.statics.build = function(id) {
    var user = new this();
    user.uid = id;
    return user;
  };

  /**
   * Finds an user with the specified id or create a new one with id
   * @param {String} id : The id of the user
   * @returns {Promise.<User>}
   **/
  usercontextSchema.statics.findOrCreate = function(id) {

    var self = this;
    return new Promise(function(resolve, reject) {
      self.findById(id, function(err, user) {
        if (err) {
          console.log(err);
          reject(err);
        }
        user ? resolve(user) : resolve(self.build(id));
      });
    });
  };

  /**
   * Create a new meetup for the user from an transaction
   * @param {Object} transaction : The transaction
   * @param {String} transaction.id : The id of the transaction
   * @param {String} transaction.buyer : The id of the buyer
   * @param {String} transaction.seller : The id of the seller
   * @param {Date}   transaction.date : The date of the transaction
   * @param {Object} transaction.location : The location of transaction
   * @param {Number} transaction.location.lat : The latitude of location
   * @param {Number} transaction.location.lon : The longitude of location
   **/
  usercontextSchema.methods.addMeetup = function(transaction) {

    if (this._id === transaction.buyer) {
      this.meetups.push({
        transaction_id: transaction.id,
        is_buyer: true,
        buyer_id: transaction.buyer,
        seller_id: transaction.seller,
        location: transaction.location,
        date: transaction.date,
        status: transaction.status,
        deliver_id: transaction.seller
      });
    } else if (this._id === transaction.seller) {
      this.meetups.push({
        transaction_id: transaction.id,
        is_buyer: false,
        buyer_id: transaction.buyer,
        seller_id: transaction.seller,
        location: transaction.location,
        date: transaction.date,
        status: transaction.status,
        deliver_id: transaction.seller
      });
    }
  };

  usercontextSchema.methods.removeMeetup = function(id) {
    _.remove(this.meetups, function(meetup) {
      return meetup.transaction_id === id;
    });
  };

  usercontextSchema.methods.updateMeetup = function(object) {
    console.log('update meetup');
    var updated = false;
    _.map(this.meetups, function(meetup) {
      if (meetup.transaction_id === parseInt(object.id)) {

        meetup = _.merge(meetup, _.pick(object, ['deliver_id', 'is_buyer', 'date', 'status', 'location', 'buyer_id', 'seller_id']))
        updated = true;
      }
      return meetup;
    });

    if (updated == false) {
      var transaction = {
        transaction_id: object.id
      };
      this.meetups.push(_.merge(transaction, _.pick(object, ['deliver_id', 'is_buyer', 'date', 'status', 'location', 'buyer_id', 'seller_id'])));
    }
  };

  usercontextSchema.methods.findMeetupByTxID = function(id) {
    var meetup_id = parseInt(id);
    return _.find(this.meetups, function(meetup) {
      return meetup.transaction_id === meetup_id;
    });
  };

  /**
   * Get scheduled meetups in the next ms amount of time
   * @param {Number} ms : The time range from now to check for meetups, in milliseconds
   * @returns {Array} : array of scheduled meetups
   **/
  usercontextSchema.methods.meetupsWithin = function(ms) {

    var now = Date.now();
    return _(this.meetups).filter(function(meetup) {
      var time_close = true;
      if (meetup.date) {
        time_close = time_close && (meetup.date.getTime() > now - ms) && (meetup.date.getTime() < now + ms) && (meetup.status === 'pending');
      } else {
        time_close = false;
      }
      return time_close;
    }).clone();
  };


  usercontextSchema.index({
    uid: 1
  }, {
    unique: true
  });


  var usercontextSchema = conn.model('Location', usercontextSchema, 'location');
  return usercontextSchema;

};
// load the things we need
var mongoose = require('mongoose');
var autoIncrement = require('mongoose-auto-increment');
var mongoosePaginate = require('mongoose-paginate');
var bcrypt = require('bcrypt-nodejs');
var Jwt = require('jsonwebtoken');
var randtoken = require('rand-token');
var _ = require('lodash');
var config = require('as-config');
var FirebaseTokenGenerator = require("firebase-token-generator");
var Firebase = require('firebase');
var tokenGenerator = new FirebaseTokenGenerator(config.firebase_secret || process.env.FIREBASE_SECRET);
var Promise = require("bluebird");
var Hashids = require('hashids');
var hashids = new Hashids(config.hashid_salt, 8, 'abcdefghijklmnopqrstuvwxyz1234567890');
var base32 = require('hi-base32');
var rp = require('request-promise');
//var models = require('as-pg').ModelPG;
var jwt = require('as-client').jwt;
var moment = require('moment-timezone');
var algoliasearch = require('algoliasearch');

// var client = algoliasearch(config.algolia.application_id, process.env.ALGOLIA_KEY || config.algolia.api_key);
// var index = client.initIndex(config.algolia.id_index);

// index.setSettings({
//   attributesToIndex: ['id', 'name', 'given_name', 'username', 'family_name', 'store_name', 'email', 'picture', 'country', 'locale', 'status', 'email_update', 'phone_update', 'is_admin', 'phone_number', 'dial_country', 'email_verified', 'phone_verified', 'password_set', 'region', 'city', 'facebook', 'local_picture', 'is_financial_admin', 'source', 'referral_code', 'email_set', 'phone_set', 'is_data_editor', 'store_id'],
//   customRanking: ['asc(id)'],
//   attributesForFaceting: ['status']
// });

var index = require('as-client').search_index.id;


var REFRESH_TOKEN_LENGTH = 24,
  ACCESS_TOKEN_EXPIRY = config.code_expire_in, // minutes
  FIREBASE_TOKEN_EXPIRY = config.firebase_token_expire_in; // minutes

module.exports = function(conn, mongoose) {

  autoIncrement.initialize(conn);
  // define the schema for our user model
  var userSchema = mongoose.Schema({

    loc: {
      type: [Number], // [<longitude>, <latitude>]
      index: '2d' // create the geospatial index
    },
    refresh_token: {
      type: String,
      index: true,
      unique: true
    },

    local: {
      email: String,
      referrer: {
        type: String,
        default: ""
      },
      password: {
        type: String
      },
      phone_number: {
        type: String
      },
      dial_country: String,
      dial_country_code: String,
      dial_country_iso2: String,
      email_verified: {
        type: Boolean,
        default: false
      },
      phone_verified: {
        type: Boolean,
        default: false
      },
      password_set: {
        type: Boolean,
        default: false
      },
      username: String,
      name: String,
      store_name: String,
      store_id: Number,
      given_name: String,
      family_name: String,
      picture: {
        type: String,
        default: 'https://antsquare.imgix.net/antsquare/as-avatar.jpg'
      },
      local_picture: {
        type: Boolean,
        default: false
      },
      country: {
        type: String,
        default: 'US'
      },
      lat: String,
      lon: String,
      region: String,
      city: String,
      locale: String,
      pass_reset: {
        temp_str: String,
        verifed: {
          type: Boolean,
          default: false
        }
      },
      email_update: {
        email: String,
        code: String,
        expiresAt: Date,
        createdAt: Date
      },
      phone_update: {
        phone: String,
        dial_country: String,
        code: String,
        expiresAt: Date,
        createdAt: Date
      },
      followings: {
        type: [Number],
        default: [1]
      },
      followers: {
        type: [Number],
        default: []
      },
      blockings: {
        type: [Number]
      },
      status: {
        type: String,
        default: 'active'
      },
      is_active: {
        type: Boolean,
        default: true
      },
      is_admin: {
        type: Boolean,
        default: false
      },
      is_financial_admin: {
        type: Boolean,
        default: false
      },
      is_data_editor: {
        type: Boolean,
        default: false
      },
      is_super_admin: {
        type: Boolean,
        default: false
      },
      address1: {
        type: String
      },
      address2: {
        type: String
      },
      fb_share_to: {
        type: String,
        default: null
      },
      referral_code: String,
      referral_link: String,
      referral_cap: {
        type: Number,
        default: 10
      }
    },
    facebook: {
      id: String,
      token: String,
      expires_in: String,
      email: String,
      name: String
    },
    twitter: {
      id: String,
      token: String,
      displayName: String,
      username: String
    },
    google: {
      id: String,
      access_token: String,
      refresh_token: String,
      email: String,
      name: String
    },
    weibo: {
      id: String,
      token: String,
      email: String,
      name: String
    },
    wechat: {
      id: String,
      token: String,
      email: String,
      name: String
    },
    baidu: {
      id: String,
      token: String,
      email: String,
      name: String
    },
    qq: {
      id: String,
      token: String,
      name: String
    },
    updated_at: {
      type: Date,
      default: Date.now
    },
    settings: {
      item_sold: {
        type: Boolean,
        default: true
      },
      offer_received: {
        type: Boolean,
        default: true
      },
      offer_cancelled: {
        type: Boolean,
        default: true
      },
      offer_accepted: {
        type: Boolean,
        default: true
      },
      item_pickup: {
        type: Boolean,
        default: true
      },
      new_inbox_message: {
        type: Boolean,
        default: true
      },
      new_comment: {
        type: Boolean,
        default: true
      }
    },
    client_ip: String,
    source: String
  });
  /**
   * Constructor
   * */


  userSchema.statics.build = function(data) {

    var user = new this();
    user.local.phone_verified = false;
    user.local.email_verified = false;
    user.refresh_token = randtoken.generate(REFRESH_TOKEN_LENGTH);
    user.local.followings = [];
    user.local.followers = [];
    if (data) {
      user = _.merge(user, data);
    }
    return user;
  };

  // userSchema.pre('save', true, function(next, done) { //true flag for parallel operation
  //   console.log('saving: ' + this.uid);
  //   this.local.referrer = hashids.encode(this.uid);
  //   next();
  //   done();
  // })



  userSchema.methods.toAlgoliaJSON = function() {

    var result = {};

    result.id = this.uid;
    result.resource_id = 'user_' + this.uid;
    result.source = this.source;
    result.type = 'user';

    if (result.status == 'inactive') {
      result.is_active = false;
    } else {
      result.is_active = true;
    }

    if (Number(result.lat) && Number(result.lon)) {

      result.location = {
        lat: result.lat,
        lon: result.lon
      };

      result._geoloc = {
        lat: Number(result.lat),
        lng: Number(result.lon)
      }

    }

    result.facebook = this.facebook;


    result.followers = _.without(result.followers, null, undefined, result.id, '' + result.id);
    result.total_followers = result.followers.length;

    result.followings = _.without(result.followings, null, undefined, result.id, '' + result.id);
    result.total_followings = result.followings.length;


    if (result.facebook) {
      result.facebook.token = null;
    }

    if (result.email_update) {
      result.email_update = _.pick(result.email_update, ['email']);
    }

    if (result.phone_update) {
      result.phone_update = _.pick(result.phone_update, ['phone', 'dial_country']);
    }

    if (result.phone_number) {
      result.phone_set = true;
    } else {
      result.phone_set = false;
    }

    if (result.email) {
      result.email_set = true;
    } else {
      result.email_set = false;
    }

    result.unix_time = moment(result.updated_at).unix();


    result = _.merge(result, _.pick(this.local, ['name', 'given_name', 'username', 'family_name', 'email', 'picture', 'country', 'locale', 'status', 'email_update', 'phone_update', 'is_admin', 'phone_number', 'dial_country', 'email_verified', 'phone_verified', 'password_set', 'region', 'city', 'is_financial_admin', 'store_name', 'local_picture', 'referral_code', 'source', 'email_set', 'phone_set', 'is_data_editor', 'store_id', 'followers', 'followings', 'is_pretty', 'total_moment_likes', 'lat', 'lon', 'blockings', 'total_followings', 'total_followers', 'location', '_geoloc']));


    return result;

  };


  /**
   * Generate a new json web token
   * @param {String} audience : The intended audience for the JWT
   * @param {String} secret : The secret to sign the JWT
   * @returns {String} : The JWT
   **/
  userSchema.methods.generateJwt = function(audience, secret) {

    var claim = {
      subject: this.uid,
      expiresInMinutes: ACCESS_TOKEN_EXPIRY,
      audience: audience,
      issuer: 'antsquare'
    };

    var playload = {
      is_admin: this.local.is_admin || false,
      is_financial_admin: this.local.is_financial_admin || false,
      is_super_admin: this.local.is_super_admin || false,
      phone_verified: this.local.phone_verified || false,
      email_verified: this.local.email_verified || false
    }

    return Jwt.sign(playload, secret, claim);

  };

  userSchema.methods.generateJwt1 = function() {

  if (config.jwt) {
    var secret = new Buffer(config.jwt.secret || process.env.JWT_SECRET, 'base64');
    return this.generateJwt(config.jwt.audience || process.env.JWT_AUDIENCE, secret);
  } else {
    var secret = new Buffer(process.env.JWT_SECRET, 'base64');
    return this.generateJwt(process.env.JWT_AUDIENCE, secret);
  }
};

  userSchema.methods.generateRefCode = function() {
    this.referral_code = this.uid.toString(36);
  };

  userSchema.methods.generateFirebaseToken = function() {
    // var timestamp = Firebase.ServerValue.TIMESTAMP;
    // console.log('firebase timestamp:');
    // console.log(timestamp);
    var token = tokenGenerator.createToken({
      uid: this.uid + ''
    }, {
      debug: true,
      expires: (parseInt(Date.now() / 1000, 10) + FIREBASE_TOKEN_EXPIRY * 60)
        //expires: (parseInt(timestamp / 1000, 10) + FIREBASE_TOKEN_EXPIRY * 60)
    });
    return token;
  };

  /**
   * Check if the user has valid credentials to facebook
   * @param {String} token : A currently valid facebook OAUTH token to check agaist
   * @returns {Boolean} : If user has valid access to facebook
   **/
  userSchema.methods.hasFacebookAccess = function(token) {
    return !!this.facebook.token && this.facebook.token === token;
  };


  userSchema.methods.linkFacebook = function(token, profile) {
    this.facebook.token = token;
    this.facebook.id = profile.id;
    this.facebook.name = profile.displayName;
    this.facebook.email = ((profile.emails && profile.emails[0] && profile.emails[0].value) ? profile.emails[0].value.toLowerCase() : null);
    if (!this.local.name) {
      this.local.name = this.facebook.name;
    }
    if (!this.local.given_name) {
      this.local.given_name = profile.name.givenName;
    }
    if (!this.local.family_name) {
      this.local.family_name = profile.name.familyName;
    }
    if (!this.local.locale) {
      this.local.locale = profile._json.locale;
    }

    if (!this.local.email && this.facebook.email) {
      this.local.email = this.facebook.email;
      this.local.email_verified = false;
    }

    if (profile.picture && !this.local.local_picture) {
      this.local.picture = profile.picture;
    }
  };

  /**
   * Check if the user has valid credentials to google plus
   * @param {String} access_token : A currently valid google plus OAUTH access token to check against
   * @returns {Boolean} : If user has valid access to google plus
   **/
  userSchema.methods.hasGoogleAccess = function(token) {
    return !!this.google.access_token && this.google.access_token === token;
  };


  userSchema.methods.linkGoogle = function(access_token, refresh_token, profile) {

    this.google.access_token = access_token;
    this.google.refresh_token = refresh_token || this.google.refresh_token;
    this.google.id = profile.id;
    this.google.name = profile.displayName;
    this.google.email = (profile.emails[0].value || '').toLowerCase();
    this.google.picture = profile.image.url;

    this.local.name = this.google.name;
    this.local.email = this.google.email;
    this.local.picture = this.google.picture;
    this.local.given_name = profile.name.givenName;
    this.local.family_name = profile.name.familyName;
    if (this.local.email) {
      this.local.email_verified = true;
    }
  };

  // generating a hash
  userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
  };

  userSchema.methods.generateReferrer = function() {
    return hashids.encode(this.uid);
  };

  // checking if password is valid
  userSchema.methods.validPassword = function(password) {
    if (!this.local.password) {
      return false;
    }
    return bcrypt.compareSync(password, this.local.password);
  };

  // checking if password is valid
  userSchema.methods.addFollower = function(uid) {
    if (this.uid == uid || this.uid + '' == uid)
      return;
    this.local.followers.addToSet(uid);
  };

  // checking if password is valid
  userSchema.methods.addFollowing = function(uid) {
    if (this.uid == uid || this.uid + '' == uid)
      return;
    this.local.followings.addToSet(uid);
  };

  // checking if password is valid
  userSchema.methods.removeFollower = function(uid) {
    this.local.followers.pull(uid);
  };

  // checking if password is valid
  userSchema.methods.removeFollowing = function(uid) {
    this.local.followings.pull(uid);
  };

  // checking if password is valid
  userSchema.methods.addBlocking = function(uid) {
    this.local.blockings.addToSet(uid);
  };

  // checking if password is valid
  userSchema.methods.removeBlocking = function(uid) {
    this.local.blockings.pull(uid);
  };



  userSchema.plugin(autoIncrement.plugin, {
    model: 'user',
    field: 'uid',
    startAt: 10000,
    incrementBy: 1
  });

  userSchema.plugin(mongoosePaginate);

  userSchema.index({
    uid: 1
  });

  userSchema.index({
    'local.email': 1,
    'email_verified': 1
  });

  userSchema.index({
    'local.phone_number': 1,
    'phone_verified': 1
  });

  userSchema.index({
    'facebook.id': 1
  });

  userSchema.post('save', function(doc) {
    console.log('save hook of user ', doc._id);

    //index = index || userSchema.getSearchIndex();
    var model = doc.toAlgoliaJSON();

    index.deleteObject('' + model.id, function(err) {
      if (err) {
        console.log(err.stack);
      }
    });

    index.deleteObject(model.id, function(err) {
      if (err) {
        console.log(err.stack);
      }
    });

    index.saveObject(_.merge({
      objectID: model.resource_id,
    }, model), function(err, content) {
      if (err) {
        console.error(err);
      } else {
        console.log('user ' + doc.uid + ' synced to index');

        jwt.decorateUserWithJwt(doc);

        var option1 = {
          uri: config.core + '/v5/users',
          method: 'POST',
          json: true,
          body: _.pick(model, ['name', 'given_name', 'username', 'family_name', 'email', 'picture', 'bio', 'locale', 'status', 'lat', 'lon', 'phone_number', 'email_verified', 'phone_verified', 'followers', 'followings', 'hashid', 'status', 'blockings', 'store_id', 'is_active', 'is_pretty']),
          headers: {

            'authorization': 'Bearer ' + doc.jwt
          }
        };
        //console.log(option1);
        rp(option1).then(function(data) {
          //console.log(data);
        }).catch(function(err) {
          console.log(err);
        });

        //Model.User.findor

        // rp(config.core + "/v4/categories").then(function(data) {
        //   //console.log('categories retrieved: ' + data);
        //   data = JSON.parse(data);
        //   var results = Object.keys(data).map(function(itemList) {
        //     return data[itemList].map(function(categoryItem) {
        //       return categoryItem.tag
        //     })
        //   })
        //   results = results.concat.apply([], results);
        //   uniqueResults = {} //deduplication of keys
        //   results.filter(function(tag) {
        //     return uniqueResults.hasOwnProperty(tag) ? false : (uniqueResults[tag] = true);

        //   })
        //   var formattedResults = results.map(function(result) {
        //     return { "user_id": doc.uid, "tag": result }
        //   })
        //   Model.Subscriptions.bulkCreate(formattedResults).then(function(data) {
        //     //console.log('subscribed new user to tags: ' + JSON.stringify(data))
        //   }).catch(function(err) {
        //     console.log("error subscribing: " + JSON.stringify(err))
        //   })
        // })

      }
    });
  })

  var user = conn.model('User', userSchema, 'user');

  return user;
};

// create the model for users and expose it to our app
//module.exports = mongoose.model('user', userSchema, 'user');
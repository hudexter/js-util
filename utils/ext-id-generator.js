
var Hashids = require("hashids"),
  _ = require('lodash'),
  FlakeIdGen = require('flake-idgen'),
  intformat = require('biguint-format'),
  generator = new FlakeIdGen();

var defaultDelimiter = "-";

module.exports = {
  /*
    Encrypt numeric ID.
    @param  options.id  The id to be encrypted.  If not a number, return null. Required.
    @param  options.salt The salt for encryption.  optional.
    @param  options.prefix The prefix of the encrypted result, followed by delimiter. optional.
    @param  options.autoPostfix  Auto generate a postfix, following a delimiter. Higher precendent than manual postfix. optional.
    @param  options.postfix  The manual postfix, following a delimiter. optional
    @param  options.delimiter  The delimiter to use to separater prefix, body and postfix.  Default to "-".  optional.
    @return alphanumeric string with delimiter.

    @Example:  encryptId({id:123, prefix:"a", autoPostfix:true, delimiter:'.'});
    return value is: a.n6P.53a4d05e1b800000
  */
  encryptId: function(options) {
    if (!options) options = {};
    if (_.isNaN(parseInt(options.id, 10))) return null;
    var d = new Date();
    var salt = options.salt || "default";

    var autoPostfix = options.autoPostfix || false;
    var id = parseInt(options.id, 10) || d.getTime();
    var delimiter = options.delimiter || defaultDelimiter;

    var gid = generator.next();
    var prefix = (options.prefix) ? options.prefix + delimiter : "";
    var postfix = (autoPostfix) ? delimiter + intformat(gid, 'hex') : (options.postfix) ? delimiter + options.postfix : "";

    var hashids = new Hashids(prefix + salt + postfix);

    return prefix + hashids.encode(id) + postfix;
  },

  /*
    Decrypt a full message and return the id from the decrypted message body.
    @param  options.message   The full message. if empty, return null. required.
    @param  options.salt      The salt for decryption. optional.
    @param  options.delimiter The delimiter for separating the prefix, body & postfix.
    @return number            The id.

    @Example: decryptId({message:'a.n6P.53a4d05e1b800000', hasPrefix:true, delimiter:'.'});
    return value is: 123
  */
  decryptId: function(options) {
    if (!options) options = {};
    if (!options.message) return null;
    var salt = options.salt || "default";
    var message = options.message || '';

    var delimiter = options.delimiter || defaultDelimiter;

    if (!message) return message;

    var mArray = message.split(delimiter);

    var hashids;
    var result = null;
    if (mArray.length == 1) {
      hashids = new Hashids(salt);
      return hashids.decode(mArray[0])[0];
    } else if (mArray.length == 2) {
      hashids = new Hashids(mArray[0] + delimiter + salt);
      result = hashids.decode(mArray[1])[0];
      if (!_.isUndefined(result)) return result;
      hashids = new Hashids(salt + delimiter + mArray[1]);
      return hashids.decode(mArray[0])[0];
    } else {
      hashids = new Hashids(mArray[0] + delimiter + salt + delimiter + mArray[2]);
      return hashids.decode(mArray[1])[0];
    }
  }
};
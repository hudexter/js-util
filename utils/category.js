
var Category = module.exports = {};


Category.getAll = function() {
  var categories;
  var product_names = [
    'accessories',
    'food',
    'antiques',
    'art, collectibles',
    'auto parts',
    'baby items',
    'bikes',
    'bike parts & accessories',
    'books',
    'office, stationary',
    'cameras, camcorders',
    'cds, dvds, blu-ray',
    'clothing',
    'computers',
    'computer accessories',
    'electronics',
    'furniture',
    'totes, handbags, backpacks',
    'health, special needs',
    'hobbies, crafts',
    'home appliances',
    'home - indoor',
    'home - outdoor',
    'home renovation materials',
    'iPods, mp3, headphones',
    'jewelry',
    'musical instruments',
    'phones',
    'sporting goods, exercise',
    'tickets',
    'tools',
    'toys, games',
    'video games, consoles',
    'watches',
    'other products'
  ];

  var product_icons = [
    'accessories',
    'other',
    'antique',
    'art',
    'autopart',
    'baby',
    'bike',
    'bikepart',
    'book',
    'business',
    'camera',
    'optical',
    'clothing',
    'computer',
    'computeraccs',
    'electronics',
    'furniture',
    'handbag',
    'health',
    'hobby',
    'homeappliance',
    'homeindoor',
    'homeoutdoor',
    'homereno',
    'mp3',
    'jewelry',
    'music',
    'phone',
    'sport',
    'ticket',
    'tool',
    'toy',
    'videogame',
    'watch',
    'other',
  ];

  var service_names = [
    'barbers & hair services',
    'childcare, nanny',
    'cleaners, cleaning',
    'computer repair',
    'IT service',
    'entertainment',
    'financial, legal',
    'fitness, personal trainer',
    'health, beauty',
    'moving, storage',
    'music lessons',
    'phone repair',
    'photography, video',
    'skilled trades',
    'travel, vacations',
    'tutors, languages',
    'wedding',
    'other services'
  ];
  var service_icons = [
    'hair',
    'childcare',
    'cleaners',
    'computerservice',
    'computer',
    'entertainment',
    'bank',
    'fitness',
    'healthservice',
    'moving',
    'musicservice',
    'phoneservice',
    'photography',
    'trades',
    'travel',
    'tutor',
    'wedding',
    'otherservice'
  ];
  var request_names = [
    'accessories',
    'food',
    'antiques',
    'art, collectibles',
    'auto parts',
    'baby items',
    'bikes',
    'bike parts & accessories',
    'books',
    'office, stationary',
    'cameras, camcorders',
    'cds, dvds, blu-ray',
    'clothing',
    'computers',
    'computer accessories',
    'electronics',
    'furniture',
    'totes, handbags, backpacks',
    'health, special needs',
    'hobbies, crafts',
    'home appliances',
    'home - indoor',
    'home - outdoor',
    'home renovation materials',
    'iPods, mp3, headphones',
    'jewelry',
    'musical instruments',
    'phones',
    'sporting goods, exercise',
    'tickets',
    'tools',
    'toys, games',
    'video games, consoles',
    'watches',
    'other products',
    'barbers & hair services',
    'childcare, nanny',
    'cleaners, cleaning',
    'computer repair',
    'IT service',
    'entertainment',
    'financial, legal',
    'fitness, personal trainer',
    'health, beauty',
    'moving, storage',
    'music lessons',
    'phone repair',
    'photography, video',
    'skilled trades',
    'travel, vacations',
    'tutors, languages',
    'wedding',
    'other requests'
  ];
  var request_icons = [
    'accessories',
    'other',
    'antique',
    'art',
    'autopart',
    'baby',
    'bike',
    'bikepart',
    'book',
    'business',
    'camera',
    'optical',
    'clothing',
    'computer',
    'computeraccs',
    'electronics',
    'furniture',
    'handbag',
    'health',
    'hobby',
    'homeappliance',
    'homeindoor',
    'homeoutdoor',
    'homereno',
    'mp3',
    'jewelry',
    'music',
    'phone',
    'sport',
    'ticket',
    'tool',
    'toy',
    'videogame',
    'watch',
    'other',
    'hair',
    'childcare',
    'cleaners',
    'computerservice',
    'computer',
    'entertainment',
    'bank',
    'fitness',
    'healthservice',
    'moving',
    'musicservice',
    'phoneservice',
    'photography',
    'trades',
    'travel',
    'tutor',
    'wedding',
    'otherrequest'
  ];



  categories = {};
  categories.products = [];
  categories.services = [];
  categories.requests = [];
  for (var i = 0; i < product_names.length; i++) {
    categories.products.push({
      'name': product_names[i],
      'icon': product_icons[i],
      'id': i
    });
  }

  for (var i = 0; i < service_names.length; i++) {
    categories.services.push({
      'name': service_names[i],
      'icon': service_icons[i],
      'id': i + product_names.length
    });
  }

  for (var i = 0; i < request_names.length; i++) {
    categories.requests.push({
      'name': request_names[i],
      'icon': request_icons[i],
      'id': i + product_names.length + service_names.length
    });
  }

  return categories;
};

Category.getAllV4 = function() {
  var categories;
  var product_names = [
    'Arts & Crafts',
    'Baby & Kids',
    'Home & Living',
    'Fashion & Accessories',
    'Sports & Recreation',
    'Jewelry',
    'Electronics',
    'Pets',
    'Food',
    'Used',
    'Other Products'
  ];

  var product_icons = [
    'art',
    'baby',
    'homeappliance',
    'accessories',
    'sport',
    'jewelry',
    'computeraccs',
    'pet',
    'health',
    'book',
    'other'
  ];

  var product_icons_urls = [
    'http://antsquare-www.imgix.net/categories/products/ic_arts&crafts.png',
    'http://antsquare-www.imgix.net/categories/products/ic_baby&kids.png',
    'http://antsquare-www.imgix.net/categories/products/ic_home&living.png',
    'http://antsquare-www.imgix.net/categories/products/ic_fashion&accesories.png',
    'http://antsquare-www.imgix.net/categories/products/ic_sports&recreation.png',
    'http://antsquare-www.imgix.net/categories/products/ic_jewelry.png',
    'http://antsquare-www.imgix.net/categories/products/ic_electronics.png',
    'http://antsquare-www.imgix.net/categories/products/ic_pets.png',
    'http://antsquare-www.imgix.net/categories/products/ic_food.png',
    'http://antsquare-www.imgix.net/categories/products/ic_used.png',
    'http://antsquare-www.imgix.net/categories/products/ic_otherproducts.png'
  ];



  var service_names = [
    'Beauty & Spa',
    'Cleaning & Landscaping',
    'Delivery',
    'Entertainment',
    'Design & Tech',
    'Hair & Barber',
    'Lessons',
    'Fitness',
    'Maintenance & Repair',
    'Photography & Events',
    'Other Service'
  ];
  var service_icons = [
    'healthservice',
    'cleaners',
    'moving',
    'entertainment',
    'computerservice',
    'hair',
    'tutor',
    'fitness',
    'trades',
    'photography',
    'otherservice'
  ];

  var service_icons_urls = [
    'https://antsquare-www.imgix.net/categories/services/ic_beauty&spa.png',
    'https://antsquare-www.imgix.net/categories/services/ic_cleaning&landscaping.png',
    'https://antsquare-www.imgix.net/categories/services/ic_Delivery.png',
    'https://antsquare-www.imgix.net/categories/services/ic_entertainment.png',
    'https://antsquare-www.imgix.net/categories/services/ic_design&tech.png',
    'https://antsquare-www.imgix.net/categories/services/ic_hair&barber.png',
    'https://antsquare-www.imgix.net/categories/services/ic_lessons.png',
    'https://antsquare-www.imgix.net/categories/services/ic_fitness.png',
    'https://antsquare-www.imgix.net/categories/services/ic_maintainence&repair.png',
    'https://antsquare-www.imgix.net/categories/services/ic_maintainence&repair.png',
    'https://antsquare-www.imgix.net/categories/services/ic_photography&events.png'
  ];

  var request_names = [
    'Arts & Crafts',
    'Baby & Kids',
    'Home & Living',
    'Fashion & Accessories',
    'Sports & Recreation',
    'Jewelry',
    'Electronics',
    'Pets',
    'Food',
    'Used',
    'Other Products',
    'Beauty & Spa',
    'Cleaning & Landscaping',
    'Delivery',
    'Entertainment',
    'Design & Tech',
    'Hair & Barber',
    'Lessons',
    'Fitness',
    'Maintenance & Repair',
    'Photography & Events',
    'Other Request'
  ];
  var request_icons = [
    'art',
    'baby',
    'homeappliance',
    'accessories',
    'sport',
    'jewelry',
    'computeraccs',
    'pet',
    'health',
    'book',
    'other',
    'healthservice',
    'cleaners',
    'moving',
    'entertainment',
    'computerservice',
    'hair',
    'tutor',
    'fitness',
    'trades',
    'photography',
    'otherrequest'
  ];

  categories = {};
  categories.products = [];
  categories.services = [];
  categories.requests = [];
  for (var i = 0; i < product_names.length; i++) {
    var tag = request_names[i].replace(" ", "");
    var tag = tag.replace("&", "And");
    categories.products.push({
      'name': product_names[i],
      'icon': product_icons[i],
      'tag': tag,
      'id': i,
      'icon_url': product_icons_urls[i]
    });
  }

  for (var i = 0; i < service_names.length; i++) {
    var tag = request_names[i].replace(" ", "");
    var tag = tag.replace("&", "And");
    categories.services.push({
      'name': service_names[i],
      'icon': service_icons[i],
      'tag': tag,
      'id': i + product_names.length,
      'icon_url': service_icons_urls[i]
    });
  }

  for (var i = 0; i < request_names.length; i++) {
    var tag = request_names[i].replace(" ", "");
    var tag = tag.replace("&", "And");
    categories.requests.push({
      'name': request_names[i],
      'icon': request_icons[i],
      'tag': tag,
      'id': i + product_names.length + service_names.length
    });
  }
  return categories;
};
Category.findCategoryV4 = function(name, type) {
  var categories = Category.getAllV4();

  var temp;

  var result = {
    name: name,
    type: type,
    id: categories['services'].length - 1,
    icon: 'other'
  };

  //if ('service'.indexOf(type) > -1) { //wat? who writes like this
  if (type == 'service') {
    temp = categories['services'];
    //console.log("findCategoryV4 services "+ JSON.stringify(temp));
  } else if (type == 'product') {
    temp = categories['products'];
    //console.log("findCategoryV4 products "+ JSON.stringify(temp));
  } else {
    temp = categories['requests'];
    //console.log("findCategoryV4 requests "+ JSON.stringify(temp));
  }
  result.type = type;
  //console.log('iterating over categories: ' + JSON.stringify(temp))
  for (var i = 0; i < temp.length; i++) {
    pair = temp[i];
    if (pair.name.indexOf(name) > -1) {
      //console.log('findCategoryV4 hit! '+ JSON.stringify(pair))
      result.name = pair.name;
      result.id = pair.id;
      result.icon = pair.icon;
      result.icon_url = pair.icon_url;
      break;
    }
  }

  return result;
};

Category.findCategory = function(name, type) {
  var categories = Category.getAll();

  var temp;

  var result = {
    name: name,
    type: type,
    id: categories[type].length - 1,
    icon: 'other'
  };

  if ('service'.indexOf(type) > -1) {
    result.type = 'service';
    temp = categories['services'];
  } else {
    result.type = 'product';
    temp = categories['products'];
  }

  for (var i = 0; i < temp.length; i++) {
    pair = temp[i];
    if (pair.name.indexOf(name) > -1) {
      result.name = pair.name;
      result.id = pair.id;
      result.icon = pair.icon;
      result.icon_url = pair.icon_url;
      break;
    }
  }
  return result;
};

Category.findCategoryByName = function(name) {
  var categories = Category.getAll();

  var temp;

  temp = categories['services'];

  for (var i = 0; i < temp.length; i++) {
    pair = temp[i];
    if (pair.name.indexOf(name) > -1) {
      return 'service';
    }
  }
  temp = categories['products'];

  for (var i = 0; i < temp.length; i++) {
    pair = temp[i];
    if (pair.name.indexOf(name) > -1) {
      return 'product';
    }
  }

  return 'product';
};

Category.findCategoryByNameV4 = function(name) {
  var categories = Category.getAllV4();

  var temp;
  temp = categories['services'];

  for (var i = 0; i < temp.length; i++) {
    pair = temp[i];
    if (pair.name.indexOf(name) > -1) {
      return 'service';
    }
  }
  temp = categories['products'];

  for (var i = 0; i < temp.length; i++) {
    pair = temp[i];
    if (pair.name.indexOf(name) > -1) {
      return 'product';
    }
  }

  return 'product';
};
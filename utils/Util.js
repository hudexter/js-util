var exports = module.exports = {};
var config = require('as-config');
var Promise = require('bluebird');

var _ = require('lodash'),
  models = require('as-pg').ModelPG,
  rp = require('request-promise'),
  Jwt = require('jsonwebtoken'),
  isNumeric = require("isnumeric"),
  env = config.name,
  util = require('util'),
  category = require('./category');

var logger = require('as-client').logger(config.slack, config.url);
var Queue = require('as-client').queue;
var Activities = require('as-activity').ModelActivity.Activities;
var SocialActivities = require('as-activity').ModelActivity.Socialactivities;
var UserFeedMeta = require('as-activity').ModelActivity.UserFeedMeta;

var ObjectId = require('mongoose').Types.ObjectId;
var NEARBY_DIST = config.nearby_distance;
var Location = require('../models_id').Location;
//var User = require('as-util').ModelID.User;

exports.updateStoreIndex = function(id) {
  var store = {};
  return models.Store.findById(id).then(function(model) {

    if (!model) {
      return Promise.reject({
        code: 404,
        message: 'store ' + id + ' not found'
      });
    }

    store = model;
    return models.Product.findAll({
      where: {
        store_id: id
      }
    });
  }).then(function(products) {
    var all = _.map(products, function(product) {

      var attrs = {};
      attrs.currency = (store.country == 'CA' ? 'cad' : 'usd');
      attrs.lat = store.lat;
      attrs.lon = store.lon;
      attrs.address1 = store.address1;
      attrs.address2 = store.address2;
      attrs.user_id = store.user_id;
      attrs.is_enabled = store.is_enabled;
      if (product.quantity <= 0) {
        attrs.quantity = 1;
      }

      if (Number(product.price) <= 0) {
        attrs.price = 1;
      }

      if (!product.category) {
        attrs.category = 'other';
      }

      if (!product.type) {
        attrs.type = 'product';
      }

      return product.updateAttributes(attrs);
    });
    return Promise.all(all);
  }).then(function(data) {
    return Promise.resolve(data);
  }).catch(function(err) {
    logger.error(err);
  });
};

exports.updateStoreFromUser = function(id) {
  var self = {};
  models.User.findById(id).then(function(user) {
    if (user) {
      self.user = user;
    }
    return models.Store.findOne({ where: { user_id: id } });
  }).then(function(store) {
    if (store) {
      self.store = store;

      var attrs = {};
      attrs.logo = self.user.picture;
      attrs.store_name = self.user.name;

      if (self.user.username && !self.store.handle) {
        attrs.handle = self.user.username;
      }

      if (self.user.followers) {
        attrs.followers = self.user.followers;
      }

      //models.Store.update(attrs, { where: { user_id: id, claimed: true } })

      self.store.updateAttributes(attrs);
    }
  }).catch(function(err) {
    logger.error(err);
  });
}

exports.updateUserFromStore = function(id) {
  var self = {};
  models.Store.findById(id).then(function(store) {
    if (store) {
      self.store = store;
    }
    return models.User.findById(self.store.user_id);
  }).then(function(user) {
    if (user) {
      self.user = user;
    }
    var attrs = {};
    attrs.picture = user.logo;
    attrs.name = user.store_name;
    self.user.updateAttributes(attrs);
  }).catch(function(err) {
    logger.error(err);
  });
}


exports.getUserData = function(user_id, option) {
  var user_info = {}
    //console.log("Getting user data: " + user_id);
  return models.UserR.findOne({ where: { id: user_id, is_active: true } }).then(function(user) {
    // if (user) {
    //   user_info = {
    //     id: parseInt(user.id, 10),
    //     xid: user.id,
    //     given_name: user.given_name,
    //     family_name: user.family_name,
    //     name: user.name,
    //     username: user.username,
    //     picture: user.picture,
    //     total_followers: user.followers.length || 0,
    //     followers: user.followers || []
    //   }
    // } else {
    //   return Promise.reject({
    //     message: "User not found: " + user_id
    //   });
    // }

    if (user) {

      user_info = _.pick(user.toElasticSearchModel(), ['id', 'name', 'username', 'picture', 'followers', 'followings', 'type', 'status', 'resource_id', 'total_moment_likes_string', 'total_followings_string', 'total_followers_string', 'total_moment_likes', 'as_business']);

      if (user_info.picture && user_info.picture.indexOf('imgix.net') > -1) {
        user_info.picture += '?auto=compress,enhance,format,redeye';
      }


      if (option && option.viewer_id) {
        option.viewer_id = parseInt(option.viewer_id);
        user_info.is_following = (user_info.followers.indexOf(option.viewer_id) > -1 ? true : false)
      } else {
        user_info.is_following = false;
      }

      user_info.followers = _.without(user_info.followers, null, undefined, user_info.id);
      user_info.total_followers = user_info.followers.length;
      user_info.followings = _.without(user_info.followings, null, undefined, user_info.id);
      user_info.total_followings = user_info.followings.length;

    } else {
      return Promise.reject({
        //message: "User not found: " + user_id
        is_active: false
      });
    }

  }).then(function() {
    return models.StoreR.findOne({ where: { user_id: user_id } });
  }).then(function(store) {
    if (store) {
      user_info.store_info = {
          id: store.id,
          store_name: store.store_name,
          logo: store.logo,
          as_business: store.as_business
        }
        //user_info.stores = [user_info.store_info];
    } else {
      user_info.store_info = {};
    }
  }).then(function() {
    return Promise.resolve(_.omit(user_info, ['followings', 'followers']));
  }).catch(function(e) {
    console.log(e);
    return Promise.resolve({ is_active: false });
  });
}

exports.getStoreData = function(sid) {
  var store_info = {}
  console.log("Getting store data: " + sid);

  return models.StoreR.findById(sid).then(function(store) {
    if (store) {
      store_info = {
        id: store.id,
        store_name: store.store_name,
        logo: store.logo,
        as_business: store.as_business
      }
    }
  }).then(function() {
    return Promise.resolve(store_info);
  }).catch(function(e) {
    console.log(e);
    return Promise.reject(store_info);
  });
}


exports.getUserProfile = function(user_id, option) {
  var profile = {}
    //console.log("Getting user data: " + user_id);
  return models.UserR.findOne({
    where: { id: user_id, is_active: true }
  }).then(function(user) {
    if (user) {
      // profile = {
      //   id: parseInt(user.id, 10),
      //   given_name: user.given_name,
      //   family_name: user.family_name,
      //   name: user.name,
      //   username: user.username,
      //   picture: user.picture,
      //   followers: user.followers || [],
      //   followings: user.followings || []
      // }

      profile = _.pick(user.toElasticSearchModel(), ['id', 'name', 'username', 'picture', 'followers', 'followings', 'type', 'status', 'resource_id', 'lat', 'lon', 'is_pretty', 'total_moment_likes', 'total_moment_likes_string', 'total_followings_string', 'total_followers_string', 'as_business', 'city']);

      if (profile.picture && profile.picture.indexOf('imgix.net') > -1) {
        profile.picture += '?auto=compress,enhance,format,redeye';
      }

      if (option && option.viewer_id) {
        option.viewer_id = parseInt(option.viewer_id);
        profile.is_following = (profile.followers.indexOf(option.viewer_id) > -1 ? true : false)
      } else {
        profile.is_following = false;
      }

      profile.followers = _.without(profile.followers, null, undefined, profile.id);
      profile.total_followers = profile.followers.length;

      profile.followings = _.without(profile.followings, null, undefined, profile.id);
      profile.total_followings = profile.followings.length;

    } else {
      return Promise.reject({
        is_active: false
      });
    }
  }).then(function() {
    return models.NoteR.count({
      where: {
        user_id: user_id,
        is_visible: true,
        approved: true,
        is_deleted: false,
        is_enabled: true
      }
    });
  }).then(function(total) {

    if (total) {
      profile.total_moments = total;
    } else {
      profile.total_moments = 0;
    }

    return models.StoreR.findOne({ where: { user_id: user_id } });
  }).then(function(store) {
    if (store) {
      profile.store_info = _.omit(store.toElasticSearchModel(), ['followers', 'followings', '_geoloc', 'center_loc', 'store_category_info', 'store_hashid', 'claim_code', 'bio', 'cover_image', 'shop_type', 'radius', 'handle', 'state', 'country', 'cash_enabled', 'card_enabled', 'store_category']);
      //profile.stores = [profile.store_info];
    }
  }).then(function() {
    return Promise.resolve(_.omit(profile, ['followings', 'followers']));
  }).catch(function(e) {
    console.log(e);
    return Promise.resolve({ is_active: false });
  });
}

exports.reverseGeocode = function(lat, lon) {

  var url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + lat + ',' + lon + '&sensor=false&key=' + config.google.api_key;
  console.log(url);

  return rp(url).then(function(response) {
    //console.log(response);
    var address = {};
    response = JSON.parse(response);
    //console.log('length: ' + response.results.length);
    if (response.results && response.results.length > 0) {
      var results = response.results;
      var formatted_address = results[0].formatted_address;
      console.log(formatted_address);
      address.address1 = formatted_address.substring(0, formatted_address.indexOf(','));
      address.address2 = formatted_address.substring(formatted_address.indexOf(',') + 1).trim();

      for (var i = 0; i < results.length; i++) {

        if (results[i].types.indexOf("country") > -1) {
          var length = results[i].address_components.length;
          address.country = results[i].address_components[length - 1].short_name;
        };

        if (results[i].types.indexOf("administrative_area_level_1") > -1) {
          var length = results[i].address_components.length;
          address.state = results[i].address_components[length - 2].short_name;
        };
      };
    }

    if (!address.country) {
      address.country = 'US';
      address.state = 'CA';
    }

    console.log(address);
    return Promise.resolve(address);
  });
};

exports.getUserMap = function(ids, option) {
  // return rp({
  //   uri: config.env().id + '/v2/api/profiles?ids=' + ids.join(',') + '&store=true',
  //   gzip: true
  // }).then(function(data) {

  var usermap = {};
  console.log(ids);
  var self = {};
  return models.UserR.findAll({ where: { id: { $in: ids } } }).then(function(users) {

    _.map(users, function(user) {
      var profile = user.toElasticSearchModel();
      if (option && option.viewer_id) {
        option.viewer_id = parseInt(option.viewer_id);
        profile.is_following = (profile.followers.indexOf(option.viewer_id) > -1 ? true : false)
      } else {
        profile.is_following = false;
      }

      usermap[user.id] = _.omit(profile, ['followings', 'followers']);
    });
    self.users = users;
    return models.StoreR.findAll({ where: { user_id: { $in: ids } } });
  }).then(function(stores) {
    var storemap = {};
    _.map(stores, function(store) {
      storemap[store.user_id] = store.toElasticSearchModel();
    });

    _.map(self.users, function(user) {
      if (storemap[user.id]) {
        //usermap[user.id].stores = [storemap[user.id]];
        usermap[user.id].store_info = storemap[user.id];
      } else {
        //usermap[user.id].stores = [];
        usermap[user.id].store_info = {};
      }
    });

    return Promise.resolve(usermap);

  }).catch(function(err) {
    if (err) {
      console.log(err);
      console.log(err.stack);
    }
    return Promise.resolve({});
  });
};

exports.getProductMap = function(ids, option) {
  // return rp({
  //   uri: config.env().id + '/v2/api/profiles?ids=' + ids.join(',') + '&store=true',
  //   gzip: true
  // }).then(function(data) {

  var productmap = {};
  console.log(ids);
  var self = {};

  return models.ProductR.findAll({ where: { id: { $in: ids } } })
    .then(function(products) {
      var productmap = {};
      _.map(products, function(product) {
        productmap[product.id] = product.toElasticSearchModel();
      });
      return Promise.resolve(productmap);
    }).catch(function(err) {
      if (err) {
        console.log(err);
        console.log(err.stack);
      }
      return Promise.resolve({});
    });
};


exports.getStoreMap = function(ids, option) {
  // return rp({
  //   uri: config.env().id + '/v2/api/profiles?ids=' + ids.join(',') + '&store=true',
  //   gzip: true
  // }).then(function(data) {

  var storemap = {};
  console.log(ids);
  var self = {};

  return models.StoreR.findAll({ where: { id: { $in: ids } } })
    .then(function(stores) {
      var storemap = {};
      _.map(stores, function(store) {
        storemap[store.id] = store.toElasticSearchModel();
      });
      return Promise.resolve(storemap);
    }).catch(function(err) {
      if (err) {
        console.log(err);
        console.log(err.stack);
      }
      return Promise.resolve({});
    });
};

exports.getUsers = function(ids, option) {
  // return rp({
  //   uri: config.env().id + '/v2/api/profiles?ids=' + ids.join(',') + '&store=true',
  //   gzip: true
  // }).then(function(data) {

  var userarray = [];
  console.log(ids);
  //var self = {};
  return models.UserR.findAll({ where: { id: { $in: ids } } }).then(function(users) {

    _.map(users, function(user) {
      var profile = user.toElasticSearchModel();
      if (option && option.viewer_id) {
        option.viewer_id = parseInt(option.viewer_id);
        profile.is_following = (profile.followers.indexOf(option.viewer_id) > -1 ? true : false)
      } else {
        profile.is_following = false;
      }
      userarray[user.id] = _.omit(profile, ['followers', 'followings']);

    });
    //self.users = users;
    return models.StoreR.findAll({ where: { user_id: { $in: ids } } });
  }).then(function(stores) {
    var storemap = {};
    _.map(stores, function(store) {
      storemap[store.user_id] = store.toElasticSearchModel();
    });

    // _.map(self.users, function(user) {
    //   if (storemap[user.id]) {
    //     userarray[user.id].stores = [storemap[user.id]];
    //   } else {
    //     userarray[user.id].stores = [];
    //   }
    // });

    return Promise.resolve(userarray);

  }).catch(function(err) {
    if (err) {
      console.log(err);
      console.log(err.stack);
    }
    return Promise.resolve({});
  });
};

exports.getNotificationFeed = function(options) {

  var sort = options.sort || 'desc';
  sort = (_.indexOf(['desc', 'asc'], sort) >= 0) ? sort : 'desc';
  // var perPage = (perPage <= 0) ? config.activities.defaultPerPage : 10;
  // perPage = (perPage > config.activities.maxPerPage) ? config.activities.maxPerPage : perPage;

  var perPage = parseInt(options.per_page) || 0;
  perPage = (perPage <= 0) ? config.activities.defaultPerPage : perPage;
  perPage = (perPage > config.activities.maxPerPage) ? config.activities.maxPerPage : perPage;

  var page = parseInt(options.page, 10) || 1;

  var q = {
    "is_active": true,
    "actor.id": { $ne: options.uid },
    "target.id": options.uid,
    verb: {
      $in: ['comment_product', 'comment_moment', 'comment_service', 'new_following', 'buy_product', 'buy_serivce', 'disapprove_product', 'store_review', 'like_service', 'like_product', 'like_moment', 'new_user_mention', 'new_store_mention']
        // comment_request, buy_request, new_chat 'new_order', 'cancel_order',
    }
  }

  var sortedOrder = (sort === 'desc') ? -1 : 1;
  var query_options = {
    page: page,
    limit: perPage,
    columns: '_id xid actor verb product message target ref published published_timestamp',
    sortBy: {
      //      aid: sortedOrder,
      published_timestamp: sortedOrder
    }
  }

  var output = {};

  return exports.activitiesPaginateAsync(q, query_options, options).then(function(rawData) {

    var totalItemCount = rawData.totalItemCount;
    var totalPageCount = rawData.totalPageCount;
    var activities = rawData.data;

    activities = _.map(activities, function(activity) {
      // console.log(activity);// var result = activity;
      activity.ui_message = exports.constructUIMessage(options.uid, activity, options);
      activity.card_size = 'small';
      return activity;
    });

    activities = _.filter(activities, function(o) {
      return o.is_active;
    });

    var d = new Date();
    output = {
      server_timestamp: d.getTime(),
      cards: activities
    };
    output.total = totalItemCount;
    output.pages = totalPageCount;
    output.page = page;
    output.per_page = perPage;

    if (totalItemCount <= 0) {
      output.has_more = false;
    } else {
      output.has_more = (totalPageCount > page) ? true : false;
    }

    return exports.getAndUpdateLastQueryTimestamp(options.uid);
  }).then(function(lastData) {
    //console.log(lastData);
    if (lastData) {
      output.last_query_timestamp = lastData.last_query_timestamp.getTime();
    } else {
      output.last_query_timestamp = null;
    }

    return Promise.resolve(output);
  }).catch(function(e) {
    console.log(e.stack);
    return Promise.reject(e);
  });
};

exports.getUserFeed = function(options) {

  var sort = options.sort || 'desc';
  sort = (_.indexOf(['desc', 'asc'], sort) >= 0) ? sort : 'desc';
  // var perPage = (perPage <= 0) ? config.activities.defaultPerPage : 10;
  // perPage = (perPage > config.activities.maxPerPage) ? config.activities.maxPerPage : perPage;

  //var perPage = parseInt(options.per_page, 10) || 30;
  var perPage = parseInt(options.per_page) || 0;
  perPage = (perPage <= 0) ? config.activities.defaultPerPage : perPage;
  perPage = (perPage > config.activities.maxPerPage) ? config.activities.maxPerPage : perPage;

  var page = parseInt(options.page, 10) || 1;

  var q = {
    "is_active": true,
    "actor.id": options.uid,
    verb: {
      $in: ['comment_product', 'comment_moment', 'comment_service', 'new_following', 'buy_product', 'buy_serivce', 'new_order', 'cancel_order', 'store_review', 'like_request', 'like_service', 'like_product', 'like_moment', 'new_moment']
        // comment_request, buy_request, 
    }
  }

  var output;
  var sortedOrder = (sort === 'desc') ? -1 : 1;
  var query_options = {
    page: page,
    limit: perPage,
    columns: '_id aid actor verb product message target ref announcement published published_timestamp is_active moment',
    sortBy: {
      published: sortedOrder
    }
  }

  return exports.activitiesPaginateAsync(q, query_options, options).then(function(rawData) {

    var totalItemCount = rawData.totalItemCount;
    var totalPageCount = rawData.totalPageCount;
    var activities = rawData.data;

    activities = _.map(activities, function(activity) {
      activity.ui_message = exports.constructUIMessage(0, activity, options);
      return activity;
    });

    activities = _.filter(activities, function(o) {
      return o.is_active;
    });

    var d = new Date();
    output = {
      status: true,
      server_timestamp: d.getTime(),
      cards: activities
    };

    output.total = totalItemCount;
    output.pages = totalPageCount;
    output.page = page;
    output.per_page = perPage;
    if (totalItemCount <= 0) {
      output.has_more = false;
    } else {
      output.has_more = (totalPageCount > page) ? true : false;
    }
    return Promise.resolve(output);
  }).catch(function(e) {
    console.log("Last Error " + e.message);
    return Promise.reject(e);
  });
};

exports.getStoreFeed = function(options) {

  var sort = options.sort || 'desc';
  sort = (_.indexOf(['desc', 'asc'], sort) >= 0) ? sort : 'desc';
  // var perPage = (perPage <= 0) ? config.activities.defaultPerPage : 10;
  // perPage = (perPage > config.activities.maxPerPage) ? config.activities.maxPerPage : perPage;
  //var perPage = parseInt(options.per_page, 10) || 30;

  var perPage = parseInt(options.per_page) || 0;
  perPage = (perPage <= 0) ? config.activities.defaultPerPage : perPage;
  perPage = (perPage > config.activities.maxPerPage) ? config.activities.maxPerPage : perPage;

  var page = parseInt(options.page, 10) || 1;

  var q = {
    "is_active": true,
    "actor.id": options.uid,
    verb: {
      $in: ['new_product', 'new_service']
        // comment_request, buy_request, 
    }
  }

  var output;
  var sortedOrder = (sort === 'desc') ? -1 : 1;
  var query_options = {
    page: page,
    limit: perPage,
    columns: '_id aid actor verb product message target ref announcement published published_timestamp is_active moment',
    sortBy: {
      published: sortedOrder
    }
  }

  return exports.activitiesPaginateAsync(q, query_options, options).then(function(rawData) {

    var totalItemCount = rawData.totalItemCount;
    var totalPageCount = rawData.totalPageCount;
    var activities = rawData.data;

    activities = _.map(activities, function(activity) {
      activity.ui_message = exports.constructUIMessage(0, activity, options);
      return activity;
    });

    activities = _.filter(activities, function(o) {
      return o.is_active;
    });

    var d = new Date();
    output = {
      status: true,
      server_timestamp: d.getTime(),
      cards: activities
    };

    output.total = totalItemCount;
    output.pages = totalPageCount;
    output.page = page;
    output.per_page = perPage;
    if (totalItemCount <= 0) {
      output.has_more = false;
    } else {
      output.has_more = (totalPageCount > page) ? true : false;
    }
    return Promise.resolve(output);
  }).catch(function(e) {
    console.log("Last Error " + e.message);
    return Promise.reject(e);
  });
};

exports.activitiesPaginateAsync = function(query, query_options, option) {
  console.log(query_options);
  console.log(query);

  var result = {};
  var self = {};
  return new Promise(function(resolve, reject) {
    Activities.paginate(query, query_options, function(err, results, pageCount, itemCount) {
      if (err) {
        reject(err);
      } else {
        result.totalPageCount = pageCount;
        result.totalItemCount = itemCount;
        self.results = results;
        //self.results = _.uniqBy(self.results, 'aid');
        resolve();
      }
    });
  }).then(function() {
    var all = _.map(self.results, function(a) {
      return exports.getActivityModel(a._id, option)
        .then(function(activity) {
          console.log(option.version);
          var temp = {};
          if (option.version == 'v5') {
            temp = _.omit(activity, ['product', 'service', 'request', 'review', 'announcement', 'moment', 'followings', '_id', '__v', 'activity_type', 'xid', 'newsevent']);
            temp.card_type = activity.target_json.card_type;
            temp.card_size = activity.target_json.card_size;

          } else {
            temp = _.omit(activity, ['target_json', 'followings', '_id', '__v', 'activity_type', 'xid']);
          } //, 'followings', '_id', '__v', 'activity_type', 'xid'


          //temp.card_type = activity.card_type;
          //temp.card_size = activity.card_size;
          return Promise.resolve(temp);
        });

    });

    return Promise.all(all);

  }).then(function(as) {
    as = _.filter(as, exports.isNotEmpty);
    // as = _.filter(as, function(a) {
    //   return a.is_active;
    // });

    result.data = _.filter(as, exports.isNotEmpty);
    return Promise.resolve(result);
  }).catch(function(err) {
    logger.error(err);
    return Promise.resolve({});
  });
};

exports.getAndUpdateLastQueryTimestamp = function(id) {
  var d = new Date();
  //console.log(UserFeedMeta.findOneAndUpdate);
  return UserFeedMeta.findOneAndUpdate({
    id: id
  }, {
    last_query_timestamp: d
  }, {
    upsert: true,
    new: false
  }).exec().then(function(lastData) {
    return Promise.resolve(lastData);
  });
}

exports.getAndUpdateLastSocialFeedQueryTimestamp = function(id) {
  var d = new Date();
  //console.log(UserFeedMeta.findOneAndUpdate);
  return UserFeedMeta.findOneAndUpdate({
    id: id
  }, {
    last_socialfeed_query_timestamp: d
  }, {
    upsert: true,
    new: false
  }).exec().then(function(lastData) {
    return Promise.resolve(lastData);
  });
}

exports.getUserSocialFeed = function(options) {
  console.log(options);
  var sort = options.sort || 'desc';
  sort = (_.indexOf(['desc', 'asc'], sort) >= 0) ? sort : 'desc';
  var perPage = parseInt(options.per_page) || 0;
  perPage = (perPage <= 0) ? config.activities.defaultPerPage : perPage;
  perPage = (perPage > config.activities.maxPerPage) ? config.activities.maxPerPage : perPage;
  var page = parseInt(options.page, 10) || 1;

  options.viewer_id = options.uid;
  options.viewer_id = parseInt(options.viewer_id);

  // var q = {
  //   is_active: true,
  //   receiver_id: options.uid,
  //   verb: { $in: ['recommended_users', 'recommended_stores', 'sponsored_product', 'sponsored_service', 'new_moment'] }
  //   // new_product, new_request, store_announcement
  // }

  if ((options.uid <= 0 || options.uid != 101)) { // ignored location for now
    return exports.getNearbyMoments(options);
  }


  //return exports.getNearbyMoments(options);

  var q = {
    "$or": [{
        is_active: true,
        receiver_id: options.uid,
        verb: {
          $in: ['recommended_users', 'recommended_stores', 'sponsored_product', 'sponsored_service', 'new_moment', 'system_notification_announcement', 'new_product', 'new_newsevent'

          ]
        }
        // new_product, new_request, store_announcement
      }
      // , {
      //   is_active: true,
      //   actor_id: {
      //     $in: [1, 101, 4321, 3641],
      //   },
      //   receiver_id: 101,
      //   verb: { $in: ['recommended_users', 'recommended_stores', 'sponsored_product', 'sponsored_service', 'new_moment'] }
      // }
    ]
  }

  //var sortedOrder = (sort === 'desc') ? -1 : 1;
  var sortedOrder = -1;
  var query_options = {
    page: page,
    limit: perPage,
    columns: '_id id receiver_id activity_id reason activity published_timestamp verb',
    sortBy: {
      published_timestamp: sortedOrder,
      //activity_id: sortedOrder
    }
  }

  var output = {};
  var uids = [1, 101, 4321, 3641];
  var new_uids = [];

  return models.SettingR.findOne({
    where: {
      key: 'users_followed_by_all'
    }
  }).then(function(setting) {
    var actor_id = 0;
    console.log('users_followed_by_all:')
    console.log(setting.value);
    if (setting) {
      uids = setting.value;
    }

    return models.UserR.findById(options.uid);
  }).then(function(user) {
    var followings = [];
    if (user) {
      followings = user.toElasticSearchModel().followings;
    }

    _.map(uids, function(id) {
      if (_.includes(followings, id) == false && id != options.uid) {
        new_uids.push(id);
      }

    });

    q['$or'].push({
      is_active: true,
      actor_id: {
        $in: new_uids,
      },
      receiver_id: 101,
      verb: { $in: ['recommended_users', 'recommended_stores', 'sponsored_product', 'sponsored_service', 'new_moment', 'new_newsevent'] }
    });

    return exports.socialactivitiesPaginateAsync(q, query_options, options);
  }).then(function(rawData) {
    var totalItemCount = rawData.totalItemCount;
    var totalPageCount = rawData.totalPageCount;
    var activities = _.map(rawData.data, function(socialactivity) {

      socialactivity.activity_json.ui_message = exports.constructUIMessage(options.uid, socialactivity.activity_json, options);

      socialactivity.activity_json.reason = socialactivity.reason;
      return socialactivity.activity_json;
    });


    //activities = _.uniqBy(activities, 'aid');

    output.cards = activities;
    output.total = totalItemCount;
    output.pages = totalPageCount;
    output.page = page;
    output.per_page = perPage;

    if (totalItemCount <= 0) {
      output.has_more = false;
    } else {
      output.has_more = (totalPageCount > page) ? true : false;
    }

    return exports.getAndUpdateLastSocialFeedQueryTimestamp(options.uid);
  }).then(function(lastData) {
    //console.log(lastData);
    if (lastData) {
      output.last_query_timestamp = lastData.last_socialfeed_query_timestamp.getTime();
    } else {
      output.last_query_timestamp = null;
    }

    return Promise.resolve(output);
  }).catch(function(e) {
    console.log("Last Error " + e.message);
    return Promise.reject(e);
  });
};

exports.socialactivitiesPaginateAsync = function(query, query_options, option) {

  console.log(JSON.stringify(query, null, ' '));
  console.log(query_options);

  var result = {};
  var self = {};
  return new Promise(function(resolve, reject) {
    SocialActivities.paginate(query, query_options, function(err, results, pageCount, itemCount) {
      if (err) {
        reject(err);
      } else {

        result.totalPageCount = pageCount;
        result.totalItemCount = itemCount;
        self.results = results;

        self.results = _.uniqBy(self.results, 'activity_id');
        resolve();
      }
    });
  }).then(function() {
    //console.log(results);
    var all = _.map(self.results, function(sc) {
      //console.log(result.deepPopulate);
      // return sc.deepPopulate('activity').then(function(activity) {
      //   //console.log(activity);
      //   return Promise.resolve(activity);
      // });

      //          var a = _.cloneDeep(result);

      return exports.getActivityModel(sc.activity, option)
        .then(function(activity) {
          console.log(option.version);
          //console.log(activity);
          if (option.version == 'v5') {
            sc.activity_json = _.omit(activity, ['product', 'service', 'request', 'review', 'announcement', 'moment', 'followings', '_id', '__v', 'activity_type', 'xid', 'newsevent']);

            sc.activity_json.card_type = activity.target_json.card_type;
            sc.activity_json.card_size = activity.target_json.card_size;
          } else {
            sc.activity_json = _.omit(activity, ['target_json', 'followings', '_id', '__v', 'activity_type', 'xid']);
          } //, 'followings', '_id', '__v', 'activity_type', 'xid'
          //console.log(sc.activity_json);

          // if (activity.is_active != sc.is_active) {
          //   Activities.findOneAsync({ "_id": new ObjectId(sc.activity) }).then(function(activity1) {
          //     activity1.is_active = activity.is_active;
          //     sc.is_active = activity.is_active;
          //     console.log(activity.is_active);
          //     console.log('update activity and socialactivity is_active from model');
          //     // console.log(activity1);
          //     // console.log(sc);
          //     activity1.saveAsync();
          //     sc.saveAsync();
          //   });
          // }


          return Promise.resolve(sc);
        });

    });

    return Promise.all(all);

  }).then(function(scs) {
    //console.log(scs);

    // var amap = {};

    // _.map(scs, function(activity) {
    //   amap[activity.aid] = activity;    // })

    // //console.log(amap);
    // var data = _.map(results, function(sc) {
    //   //console.log(sc);
    //   //console.log(amap[sc.activity_id]);
    //   sc.activity = amap[sc.activity_id];
    //   return sc;
    // });


    // console.log(data);
    scs = _.filter(scs, exports.isNotEmpty);
    // scs = _.filter(scs, function(a) {
    //   return a.is_active;
    // });

    result.data = scs;
    return Promise.resolve(result);
  }).catch(function(err) {
    logger.error(err);
    return Promise.resolve({});
  });
};

exports.isStoreHandle = function(input) {

  console.log('checking is store handle: ' + input);
  if (!input) {
    return false;
  }

  if (input.indexOf('$') != 0) {
    return false;
  }

  var username = input.substring(1);

  try {
    var er = /^[A-Za-z0-9_]{1,15}$/;
    return er.test(username);
  } catch (e) {
    console.error(e);
    return false;
  }
};

exports.isUsername = function(input) {

  console.log('checking is username : ' + input);
  if (!input) {
    return false;
  }

  if (input.indexOf('@') != 0) {
    return false;
  }

  var username = input.substring(1);

  try {
    var er = /^[A-Za-z0-9_]{1,15}$/;
    return er.test(username);
  } catch (e) {
    console.error(e);
    return false;
  }
};

exports.isInteger = function(input) {

  console.log('checking is integer: ' + input);
  if (!input) {
    return false;
  }
  try {
    var er = /^-?[0-9]+$/;
    return er.test(input);
  } catch (e) {
    console.error(e);
    return false;
  }
};


exports.getStoredActivityModel = function(_id, option) {
  return Activities.findOneAsync({ "_id": new ObjectId(_id) }).then(function(activity1) {
    return Promise.resolve(activity1.toJSON());
  });
}

exports.getActivityModel = function(_id, option) {
  // console.log('getActivityModel');
  // console.log(_id);
  var self = {};
  var result = {};
  return Activities.findOneAsync({ "_id": new ObjectId(_id) }).then(function(activity1) {

    if (!activity1) {
      return Promise.reject(new Error('activity is not found.'));
    }

    result = activity1.toJSON();
    self.activity = activity1;

    return exports.getUserData(self.activity.actor.id, { viewer_id: option.viewer_id });

  }).then(function(user) {

    if (!user || user.is_active === false) {
      throw new Error('user is not found.');
    }

    result.actor = user;
    return exports.getUserData(self.activity.target.id);
  }).then(function(user) {

    if (!user || user.is_active === false) {
      throw new Error('user is not found.');
    }

    result.target = user;
    var activity = self.activity;
    if (activity.product && activity.product.id) {
      option.product_id = activity.product.id;
      delete option.moment_id;
      delete option.request_id;
      delete option.newsevent_id;

      return exports.getFeedModel(option).then(function(data) {
        result.product = data;
        result.target_json = data;
        result.card_size = data.card_size || 'big';
        result.card_type = data.card_type;
        result.is_active = data.is_active || false;
        // delete result.target_json.card_type;
        // delete result.target_json.card_size;
        return Promise.resolve(result);
      })

    } else if (activity.moment && activity.moment.id) {
      option.moment_id = activity.moment.id;
      delete option.product_id;
      delete option.request_id;
      delete option.newsevent_id;

      return exports.getFeedModel(option).then(function(data) {
        result.moment = data;
        result.target_json = data;
        result.card_size = data.card_size || 'big';
        result.card_type = data.card_type;
        result.is_active = data.is_active || false;
        //delete result.target_json.card_type;
        //delete result.target_json.card_size;

        return Promise.resolve(result);
      });

    } else if (activity.request && activity.request.id) {
      option.request_id = activity.request.id;
      delete option.product_id;
      delete option.moment_id;
      delete option.newsevent_id;

      return exports.getFeedModel(option).then(function(data) {
        result.request = data;
        result.target_json = data;
        result.card_size = data.card_size || 'big';
        result.card_type = data.card_type;
        result.is_active = data.is_active || false;
        // delete result.target_json.card_type;
        // delete result.target_json.card_size;
        return Promise.resolve(result);
      });
    } else if (activity.newsevent && activity.newsevent.id) {
      option.newsevent_id = activity.newsevent.id;
      delete option.product_id;
      delete option.moment_id;
      delete option.request_id;

      return exports.getFeedModel(option).then(function(data) {
        result.request = data;
        result.target_json = data;
        result.card_size = data.card_size || 'big';
        result.card_type = data.card_type;
        result.is_active = data.is_active || false;
        // delete result.target_json.card_type;
        // delete result.target_json.card_size;
        return Promise.resolve(result);
      });
    } else {
      delete option.product_id;
      delete option.moment_id;
      delete option.request_id;
      delete option.newsevent_id;


      var data = {
        card_size: 'small',
        card_type: self.activity.verb,
        is_active: true
      };

      result.target_json = data;
      result.card_size = data.card_size || 'big';
      result.card_type = data.card_type;
      result.is_active = data.is_active;
      // delete result.target_json.card_type;// delete result.target_json.card_size;
      return Promise.resolve(result);

    }
  }).catch(function(err) {
    console.log(err.stack);
    return Promise.resolve({});
  });
}

exports.getCommentModel = function(option) {
  var comment_id = option.comment_id || 0;
  var temp = {};
  return models.CommentR
    .findById(comment_id).then(function(model) {
      temp = model.toJSON();
      return exports.getUserProfile(model.user_id);
    }).then(function(user_info) {
      temp.user_info = _.pick(user_info, ['name', 'picture', 'id', 'username']);
      return Promise.resolve(_.pick(temp, ['id', 'content', 'user_info', 'created_at']));
    }).catch(function(err) {
      console.log(err);
      console.log(err.stack);
      return Promise.resolve({});
    });
}

exports.getStoreFeedModel = function(option) {


  var type = option.type;
  var store_id = option.store_id || 0;

  var last_top = (option.last_top === "true" || option.last_top == true);

  var model = {};
  var self = {};
  return new Promise(function(resolve, reject) {

    if (store_id) {
      resolve(models.StoreR.findById(store_id));
    } else {
      reject({ is_active: false });
    }
  }).then(function(result) {

    if (!result) {
      return Promise.reject(new Error('store is not found: ') + JSON.stringify(option));
    }

    model = result.toElasticSearchModel();
    return exports.getUserProfile(model.user_id, { viewer_id: option.viewer_id });
  }).then(function(user_info) {
    model.user_info = _.pick(user_info, ['name', 'picture', 'id', 'is_following', 'username', 'total_followers']);

    if (_.isEmpty(model.user_info)) {
      model.user_info.id = model.user_id;
      model.user_info.name = model.store_name;
      model.user_info.picture = model.logo;
      model.user_info.total_followers = 0;
      model.user_info.total_followings = 0;
      model.user_info.is_following = false;
      model.user_info.username = model.handle;
    }

    model.is_following = model.user_info.is_following;

    if (model.lat && model.lon && option.lat && option.lon) {
      var distance1 = exports.distance(model.lat, model.lon, option.lat, option.lon);
      model.distance_in_km = String(distance1) + " km";
      model.distance_in_miles = String(round(distance1 * 0.621371)) + " mi";
    } else {
      model.distance_in_km = null;
      model.distance_in_miles = null;
    }

    model.card_size = option.card_size || 'big';
    model.is_active = model.is_enabled;
    model.card_type = model.object_type;


    model.user_info.as_business = model.as_business;

    return models.DealR
      .findAndCountAll({
        where: {
          store_id: model.id,
          is_active: true
        },
        order: [
          ['created_at', 'DESC']
        ],
        offset: 0,
        limit: 5
      });
  }).then(function(data) {
    model.total_coupons = data.count;
    self.data = data;
    //   var ids = [];
    //   _.map(data.rows, function(model) {
    //     ids.push(model.user_id);
    //   });
    //   return exports.getUserMap(ids);
    // }).then(function(usermap) {
    var coupons = _.map(self.data.rows, function(coupon) {
      var temp = coupon.toElasticSearchModel();
      temp.store_info = _.pick(model, ['id', 'store_name', 'store_category']);
      return models.Dealclaim.findOne({ where: { device_id: option.device_id, coupon_id: coupon.id } }).then(function(claim) {
        if (claim && option.device_id) {
          temp.is_claimed = true;
        } else {
          temp.is_claimed = false;
        }
        return Promise.resolve(temp);
      });
    });

    return Promise.all(coupons);
  }).then(function(coupons) {
    model.last5coupons = _.without(coupons, null);

    if (option.device_id) {
      return models.Following.findOne({ where: { store_id: model.id, device_id: option.device_id, is_deleted: false } });
    }

  }).then(function(following) {

    if (following) {
      model.user_info.is_following = true;
      model.is_following = true;
    }

    return Promise.resolve(_.pick(model, ['id', 'resource_id', 'type',
      'logo', 'store_name', 'description', 'type', 'cover_image',
      'user_id', 'user_info',
      'store_category',
      'lat', 'lon', 'address1', 'address2', '_geoloc', 'location',
      'reviewed', 'is_enabled', 'is_deleted', 'approved', 'flagged', 'is_ugly', 'is_active', 'is_pretty', 'is_configured', 'claimed',
      'external_url', 'external_source', 'business_phone', 'yelp', 'website_url', 'google',
      'created_at', 'is_following',
      'average_rating', 'total_review',
      'distance_in_km', 'distance_in_miles', 'card_size', 'card_type',
      'total_coupons', 'last5coupons'
    ]));
  }).catch(function(err) {
    console.log(err);
    console.log(err.stack);
    return Promise.resolve({ is_active: false });
  });
};


exports.getUserFeedModel = function(option) {


  var user_id = option.user_id || 0;
  var model = {};

  return exports.getUserProfile(user_id, { viewer_id: option.viewer_id })
    .then(function(user_info) {

      model = _.pick(user_info, ['name', 'picture', 'id', 'is_following', 'username', 'total_followers', 'resource_id', 'type', 'lat', 'lon', 'is_pretty', 'city']);
      //model.is_following = model.user_info.is_following;

      if (model.lat && model.lon && option.lat && option.lon) {
        var distance1 = exports.distance(model.lat, model.lon, option.lat, option.lon);
        model.distance_in_km = String(distance1) + " km";
        model.distance_in_miles = String(round(distance1 * 0.621371)) + " mi";
      } else {
        model.distance_in_km = null;
        model.distance_in_miles = null;
      }

      model.card_size = option.card_size || 'big';
      model.is_active = !(model.status == 'inactive');
      model.card_type = model.type;

      return Promise.resolve(_.pick(model, ['id', 'resource_id',
        'name', 'picture', 'username',
        'lat', 'lon', 'address1', 'address2', '_geoloc', 'location',
        'status', 'approved', 'flagged', 'is_ugly', 'is_active', 'is_pretty', 'type', 'city',
        'created_at', 'is_following', 'total_followers',
        'distance_in_km', 'distance_in_miles', 'card_size', 'card_type'
      ]));
    }).catch(function(err) {
      console.log(err);
      console.log(err.stack);
      return Promise.resolve({ is_active: false });
    });
};


exports.getFeedModel = function(option) {
  //console.log('getFeedModel: ' + JSON.stringify(option));
  var type = option.type;
  var product_id = option.product_id || 0;
  var moment_id = option.moment_id || 0;
  var request_id = option.request_id || 0;
  var newsevent_id = option.newsevent_id || 0;

  var last_top = (option.last_top === "true" || option.last_top == true);

  var model = {};
  var self = {};
  return new Promise(function(resolve, reject) {

    if (product_id) {
      resolve(models.ProductR.findById(product_id));
    } else if (moment_id) {
      resolve(models.NoteR.findById(moment_id));
    } else if (newsevent_id) {
      resolve(models.EventR.findById(newsevent_id));
    } else {
      reject({ is_active: false });
    }
  }).then(function(result) {

    if (!result) {
      return Promise.reject(new Error('product or moment is not found: ') + JSON.stringify(option));
    }

    model = result.toElasticSearchModel();

    model.images = _.map(model.images, function(image) {
      if (image && image.indexOf('imgix.net') > -1) {
        return image + '?auto=compress,enhance,format,redeye'
      } else {
        return image;
      }
    });

    return exports.getUserProfile(model.user_id, { viewer_id: option.viewer_id });
  }).then(function(user_info) {

    // if (user_info.picture && user_info.picture.indexOf('imgix.net') > -1) {
    //   user_info.picture += '?q=50';
    // }

    model.user_info = _.pick(user_info, ['name', 'picture', 'id', 'is_following', 'username', 'store_info', 'total_followers', 'total_moment_likes', 'total_moment_likes_string', 'total_followings_string', 'total_followers_string', 'total_moment_likes']);
    model.is_following = model.user_info.is_following;


    //delete model.user_info.is_following;
    //   return models.Store.findById(model.store_id);
    // }).then(function(store) {
    //   if (store) {
    //     model.store_info = _.pick(store.toElasticSearchModel(), ['store_name', 'as_business', 'logo', 'id', 'cover_image', 'total_review', 'average_rating', 'handle']);
    //     model.store_info.is_following = model.user_info.is_following;
    //   } else {
    //     model.store_info = {};
    //   }

    if (model.tagged_store_id) {
      return models.StoreR.findById(model.tagged_store_id);
    }
  }).then(function(store) {
    if (store) {
      model.tagged_store_info = store.toElasticSearchModel();
    } else {
      model.tagged_store_info = null;
    }

    if (product_id) {
      return models.CommentR
        .findAndCountAll({
          where: {
            product_id: product_id,
            type: 'comment'
          },
          order: [
            ['created_at', 'DESC']
          ],
          offset: 0,
          limit: 2
        });
    } else if (moment_id) {
      return models.CommentR
        .findAndCountAll({
          where: {
            moment_id: moment_id,
            type: 'comment'
          },
          order: [
            ['created_at', 'DESC']
          ],
          offset: 0,
          limit: 2
        });
    } else if (newsevent_id) {
      return models.CommentR
        .findAndCountAll({
          where: {
            newsevent_id: newsevent_id,
            type: 'comment'
          },
          order: [
            ['created_at', 'DESC']
          ],
          offset: 0,
          limit: 2
        });
    } else {
      reject({});
    }
  }).then(function(data) {
    model.total_comments = data.count;
    model.total_comments_string = exports.abbreviateNumber(model.total_comments);
    self.data = data;
    var ids = [];
    _.map(data.rows, function(model) {
      ids.push(model.user_id);
    });
    return exports.getUserMap(ids);
  }).then(function(usermap) {

    var comments = _.map(self.data.rows, function(model) {
      var temp = model.toJSON();
      temp.user_info = _.pick(usermap[model.user_id], ['name', 'picture', 'id', 'username']);
      return _.pick(temp, ['id', 'content', 'user_info', 'created_at']);
    });

    if (option.last_top == true) {
      model.last5comments = _.without(comments, null);
    } else {
      model.last5comments = _.without(comments.reverse(), null);
    }

    option.viewer_id = parseInt(option.viewer_id);

    model.is_liked = false;
    if (model.likers && model.likers.indexOf(option.viewer_id) != -1) {
      model.is_liked = true;
    }

    // if (product_id) {
    //   if (model.type == 'product') {
    //     model.object_type = 'product';
    //   } else {
    //     model.object_type = 'service';
    //   }
    // } else if (moment_id) {
    //   model.object_type = 'moment';
    // } else if (request_id) {
    //   model.object_type = 'request';
    // } else {
    //   model.object_type = 'product';
    // }

    if (model.lat && model.lon && option.lat && option.lon) {
      var distance1 = exports.distance(model.lat, model.lon, option.lat, option.lon);
      model.distance_in_km = String(distance1) + " km";
      model.distance_in_miles = String(round(distance1 * 0.621371)) + " mi";
    } else {
      model.distance_in_km = null;
      model.distance_in_miles = null;
    }

    if (option.version == 'v3') {
      console.log('version 5 resource model in feed card')
      var result = {};
      result.user_info = model.user_info;
      result.store = model.store_info;
      result.data = _.pick(model, ['resource_id', 'object_type', 'id', 'hashid', 'object_type',
        'title', 'name', 'description', 'price_in_cents', 'price', 'price_type', 'type', 'currency',
        'store_id', 'store_name', 'user_id',
        'category', 'categoryId', 'category_info',
        'lat', 'lon', 'address1', 'address2', '_geoloc', 'location',
        'is_visible', 'reviewed', 'is_enabled', 'is_deleted', 'approved', 'flagged', 'is_ugly', 'quantity',
        'images', 'has_image', 'video_url', 'video_thumbnail', 'star',
        'random', '_tags', 'tags',
        'created_at', "unix_time", 'expires_at', 'expires_unix_time',
        'likers', 'tagged_store_id', 'start', 'end', 'url', 'tagged_store_info'
      ]);

      result = _.merge(result, _.pick(model, [
        'total_likes', 'total_likes_string',
        'total_comments', 'last5comments', 'total_comments_string',
        'is_liked', 'is_following',
        'distance_in_km', 'distance_in_miles'
      ]));

      result.card_size = option.card_size || 'big';
      model.card_type = model.object_type;

      result.is_active = result.is_enabled && !result.is_deleted && result.is_visible && result.approved

      return Promise.resolve(result);
    } else {

      model.card_size = option.card_size || 'big';
      model.is_active = model.is_enabled && !model.is_deleted && model.is_visible && model.approved;

      model.card_type = model.object_type;

      return Promise.resolve(_.pick(model, ['resource_id', 'id', 'object_type', 'title', 'name', 'description', 'price_in_cents', 'price', 'price_type', 'type', 'currency', 'store_id', 'store_name', 'user_id', 'user_info',
        'lat', 'lon', 'address1', 'address2', 'location',
        'is_visible', 'reviewed', 'is_enabled', 'is_deleted', 'approved', 'flagged', 'is_ugly', 'quantity',
        'images', 'has_image', 'video_url', 'video_thumbnail', 'star',
        'created_at', "unix_time", 'expires_at', 'expires_unix_time',
        'total_likes', 'total_likes_string',
        'total_comments', 'last5comments', 'total_comments_string',
        'is_liked', 'is_following',
        'distance_in_km', 'distance_in_miles', 'store_info', 'card_size', 'is_active', 'card_type', 'tagged_store_id', 'tagged_store_info',
        'start', 'end', 'url' //'category', 'categoryId', 'category_info', _geoloc, hashid, likers, 'random', '_tags', 'tags',
      ]));
    }
  }).catch(function(err) {
    console.log(err);
    console.log(err.stack);
    return Promise.resolve({ is_active: false });
  });
};

var round = function(x) {
  return Math.round(x * 10) / 10;
}

var deg2rad = function(deg) {
  return deg * (Math.PI / 180)
}

exports.distance = function(lat1, lon1, lat2, lon2) {

  var R = 6373; // km
  var φ1 = deg2rad(lat1);
  var φ2 = deg2rad(lat2);
  var λ1 = deg2rad(lon1);
  var λ2 = deg2rad(lon2);
  var Δφ = φ2 - φ1;
  var Δλ = λ2 - λ1;

  var a = Math.sin(Δφ / 2) * Math.sin(Δφ / 2) +
    Math.cos(φ1) * Math.cos(φ2) *
    Math.sin(Δλ / 2) * Math.sin(Δλ / 2);

  a = Math.pow(Math.sin(Δφ / 2), 2) + Math.cos(φ1) * Math.cos(φ2) * Math.pow(Math.sin(Δλ / 2), 2);
  // var a = Math.sin(Δφ / 2) * Math.sin(Δφ / 2) + Math.cos(φ1) * Math.cos(φ2) * Math.sin(Δλ / 2) * Math.sin(Δλ / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c;
  return round(d); //km
}

exports.constructUIMessage = function(uid, activity, option) {
  var uiMessage = '';
  var object = {};
  try {
    switch (activity.verb) {
      case 'new_product':
        if (option.version == 'v5') {
          object = activity.target_json || {};
        } else {
          object = activity.product || {};
        }
        uiMessage = "posted a new item " + object.name + '.';
        break;
      case 'new_moment':
        if (option.version == 'v5') {
          object = activity.target_json || {};
        } else {
          object = activity.moment || {};
        }
        object.name = '';
        uiMessage = "posted a new moment" + object.name + '.';
        break;
      case 'disapprove_product':

        if (option.version == 'v5') {
          object = activity.target_json || {};
        } else {
          object = activity.product || {};
        }
        uiMessage = "Your item " + object.name + " have been removed for a violation.";
        break;
      case 'disapprove_moment':

        if (option.version == 'v5') {
          object = activity.target_json || {};
        } else {
          object = activity.moment || {};
        }
        object.name = '';
        uiMessage = "Your moment" + object.name + " have been removed for a violation.";
        break;
      case 'like_product':

        if (option.version == 'v5') {
          object = activity.target_json || {};
        } else {
          object = activity.product || {};
        }

        if (uid == activity.target.id) {
          uiMessage = "liked your product " + object.name + '.';
        } else {
          var targetName = (activity.target.store_info && activity.target.store_info.as_business) ? activity.target.store_info.store_name : activity.target.name;
          uiMessage = "liked " + targetName + "'s item " + object.name + '.';
        }
        break;
      case 'like_service':

        if (option.version == 'v5') {
          object = activity.target_json || {};
        } else {
          object = activity.product || {};
        }

        if (uid == activity.target.id) {
          uiMessage = activity.actor.name + " liked your service " + object.name + '.';
        } else {
          var targetName = (activity.target.store_info && activity.target.store_info.as_business) ? activity.target.store_info.store_name : activity.target.name;
          uiMessage = "liked " + targetName + "'s item " + object.name + '.';
        }
        break;
        // case 'like_request':
        //   if (option.version == 'v5') {
        //     object = activity.target_json;
        //   } else {
        //     object = activity.request;
        //   }
        //   if (uid == activity.target.id) {
        //     uiMessage = activity.actor.name + " liked your request " + object.name;
        //   } else {
        //     var targetName = activity.target.name // request is always from user, not store
        //     uiMessage = activity.actor.name + " liked " + targetName + "'s request " + object.name;
        //   }
        //   break;
      case 'like_moment':

        if (option.version == 'v5') {
          object = activity.target_json || {};
        } else {
          object = activity.moment || {};
        }

        object.name = '';

        if (uid == activity.target.id) {
          uiMessage = "liked your moment" + object.name + '.';
        } else {
          var targetName = activity.target.name // request is always from user, not store
          uiMessage = "liked " + targetName + "'s moment" + object.name + '.';
        }
        break;
      case 'buy_product':

        if (option.version == 'v5') {
          object = activity.target_json || {};
        } else {
          object = activity.product || {};
        }

        if (uid == activity.target.id) {
          uiMessage = "bought your product " + object.name + '.';
        } else {
          var targetName = (activity.target.store_info && activity.target.store_info.as_business) ? activity.target.store_info.store_name : activity.target.name;
          uiMessage = "bought " + targetName + "'s product " + object.name + '.';
        }
        break;
      case 'comment_product':
        if (option.version == 'v5') {
          object = activity.target_json || {};
        } else {
          object = activity.product || {};
        }

        if (uid == activity.target.id) {
          uiMessage = " commented on your product " + object.name + '.';
        } else {
          var targetName = (activity.target.store_info && activity.target.store_info.as_business) ? activity.target.store_info.store_name : activity.target.name;
          uiMessage = " commented on " + targetName + "'s item " + object.name + '.';
        }
        break;
      case 'comment_service':
        if (option.version == 'v5') {
          object = activity.target_json || {};
        } else {
          object = activity.product || {};
        }

        if (uid == activity.target.id) {
          uiMessage = activity.actor.name + " commented on your service " + object.name + '.';
        } else {
          var targetName = (activity.target.store_info && activity.target.store_info.as_business) ? activity.target.store_info.store_name : activity.target.name;
          uiMessage = activity.actor.name + " commented on " + targetName + "'s item " + object.name + '.';
        }
        break;
      case 'comment_moment':
        if (option.version == 'v5') {
          object = activity.target_json || {};
        } else {
          object = activity.moment || {};
        }
        object.name = '';
        if (uid == activity.target.id) {
          uiMessage = "commented on your moment" + object.name + '.';
        } else {
          var targetName = (activity.target.store_info && activity.target.store_info.as_business) ? activity.target.store_info.store_name : activity.target.name;
          uiMessage = "commented on " + targetName + "'s moment" + object.name + '.';
        }
        break;
      case 'new_user_mention':
        if (option.version == 'v5') {
          object = activity.target_json || {};
        } else {
          object = activity.moment || {};
        }
        object.name = '';
        if (uid == activity.target.id) {
          uiMessage = "mentioned you on your " + object.type + "" + object.name + '.';
        } else {
          var targetName = (activity.target.store_info && activity.target.store_info.as_business) ? activity.target.store_info.store_name : activity.target.name;
          uiMessage = "mentioned you on " + targetName + "'s " + object.type + "" + object.name + '.';
        }
        break;
      case 'new_store_mention':
        if (option.version == 'v5') {
          object = activity.target_json || {};
        } else {
          object = activity.moment || {};
        }
        object.name = '';
        if (uid == activity.target.id) {
          uiMessage = "mentioned you on your " + object.type + "" + object.name + '.';
        } else {
          var targetName = (activity.target.store_info && activity.target.store_info.as_business) ? activity.target.store_info.store_name : activity.target.name;
          uiMessage = "mentioned you on " + targetName + "'s " + object.type + "" + object.name + '.';
        }
        break;

      case 'new_order':
        var meetupTime = new Date(activity.ref.extra.meetup_time);
        var meetupTimeStr = meetupTime.toLocaleString();
        uiMessage = "purchased your item " + activity.product.name + " Delivery/Pickup has been scheduled for " + meetupTimeStr + '.';
        break;
        // case 'cancel_order':
        //   var meetupTime = new Date(activity.ref.extra.meetup_time);
        //   var meetupTimeStr = meetupTime.toLocaleString();
        //   uiMessage = "Your sale of " + activity.product.name + " scheduled for " + meetupTimeStr + " has been cancelled";
        //   break;
        // case 'new_chat':
        //   uiMessage = activity.actor.name + " sent you a message " + activity.message;
        //   break;
      case 'new_following':
        console.log("UI MESSAGE UID: " + uid + " TARGET: " + JSON.stringify(activity.target))
        if (uid == activity.target.id) {
          uiMessage = "started following you.";
        } else {
          var targetName = (activity.target.store_info && activity.target.store_info.as_business) ? activity.target.store_info.store_name : activity.target.name;
          uiMessage = "started following " + targetName + '.';
        }
        break;
      case 'store_review':
        uiMessage = "has left you a review.";
        break;
        // case 'store_announcement':
        //   uiMessage = "New store promotion!"
        //   break;
      default:
        console.log("NO VERB! " + activity.verb);
        break;
    }
    return uiMessage;
  } catch (err) {
    console.log(err.stack);
    return '';
  }
}

exports.isNotEmpty = function(a) {

  if (_.isObject(a)) {
    return !_.isEmpty(a);
  } else {
    return (a === undefined || a == null || a.length <= 0) ? false : true;
  }
}

exports.scanMentions = function(text, actor_id, option) {
  ///^[A-Za-z0-9_]{1,15}$/
  var usernames = text.match(/@[A-Za-z0-9_]{1,15}/g);
  usernames = _.uniq(usernames);
  console.log(usernames);
  var activities1 = _.map(usernames, function(username) {
    if (exports.isUsername(username)) {
      return models.UserR.findOne({ where: { username: username } }).then(function(user) {
        if (user) {
          return Promise.resolve(_.merge({
            actor_id: actor_id,
            verb: 'new_user_mention',
            target_id: user.id
          }, option));
        } else {
          return Promise.resolve({});
        }
      });
    } else {
      return Promise.resolve({});
    }
  });

  var handles = text.match(/\$[A-Za-z0-9_]{1,15}/g);
  //handles = _.uniq(handles);
  console.log(handles);
  var activities2 = _.map(handles, function(handle) {
    if (exports.isStoreHandle(handle)) {
      return models.StoreR.findOne({ where: { handle: handle } }).then(function(store) {
        if (store) {
          return Promise.resolve(_.merge({
            actor_id: actor_id,
            verb: 'new_store_mention',
            target_id: store.user_id
          }, option));
        } else {
          return Promise.resolve({});
        }
      });
    } else {
      return Promise.resolve({});
    }
  });


  var all = activities1.concat(activities2);
  Promise.all(all).then(function(activities) {
    Queue.fireActvityEvents(activities);
  }).catch(function(err) {
    console.log(err.stack);
  });
}

exports.scanTags = function(text) {

  if (!text) {
    return [];
  }

  var tags = text.match(/#[A-Za-z0-9_]{1,15}/g);
  tags = _.uniq(tags);
  tags = _.map(tags, function(tag) {
    return tag.substring(1);
  });
  tags = tags.filter(Boolean);

  console.log(tags);
  return tags;
}

exports.addTags = function(option, tags) {

  var resource_id = '';
  return new Promise(function(resolve, reject) {

    if (option.product_id) {
      resolve(models.Product.findById(option.product_id));
    } else if (option.moment_id) {
      resolve(models.Note.findById(option.moment_id));
    } else {
      reject({});
    }
  }).then(function(model) {

    if (!model) {
      return Promise.reject(new Error('product or moment is not found: ') + JSON.stringify(option));
    }

    resource_id = model.resource_id;
    var old_tags = model._tags || [];
    if (tags.length > 0) {
      var tags_arry = old_tags.concat(tags);
      tags_arry = _.uniq(tags_arry);
      model.updateAttributes({ _tags: tags_arry });

      var all = _.map(tags, function(tag) {
        if (_.includes(old_tags, tag) == false) {
          return models.Tagging.create({
            tag: tag,
            resource_id: resource_id
          }).then(function(tagging) {
            return models.Tag.findOne({ where: { tag: tag } }).then(function(temp) {
              console.log(temp);
              if (temp) {
                var count = temp.count;
                return temp.updateAttributes({ count: count + 1 });
              } {
                return models.Tag.create({ tag: tag, count: 1 });
              }
            });
          });
        } else {
          return Promise.resolve({});
        }
      });
      return Promise.all(all);
    }
  }).then(function() {
    console.log('updated tags.');
    return Promise.resolve({});
  }).catch(function(err) {
    console.log(err.stack);
    return Promise.resolve({});
  });
}

exports.getNearbyMoments1 = function(options) {

  var lat = options.lat;
  var lon = options.lon;

  if (!options.lat || !options.lon) {
    console.log('# of nearby moment:' + options.lat + ' ' + options.lat);
    console.log(0);
    return Promise.resolve([]);
  }

  var maxDistance = NEARBY_DIST || 40000;

  console.log('distance range in km: ' + maxDistance);
  maxDistance /= 111.12;

  var sort = options.sort || 'desc';
  sort = (_.indexOf(['desc', 'asc'], sort) >= 0) ? sort : 'desc';
  // var perPage = (perPage <= 0) ? config.activities.defaultPerPage : 10;
  // perPage = (perPage > config.activities.maxPerPage) ? config.activities.maxPerPage : perPage;

  var perPage = parseInt(options.per_page) || 0;
  perPage = (perPage <= 0) ? config.activities.defaultPerPage : perPage;
  perPage = (perPage > config.activities.maxPerPage) ? config.activities.maxPerPage : perPage;

  var page = parseInt(options.page, 10) || 1;

  var q = {
    "is_active": true,
    // "actor.id": { $ne: options.uid },
    // "target.id": options.uid,
    verb: {
      $in: ['new_moment', 'system_notification_announcement', 'new_product', 'new_newsevent'
          //      'comment_moment', 'comment_service', 'new_following', 'buy_product', 'buy_serivce', 'disapprove_product', 'store_review', 'like_service', 'like_product', 'like_moment', 'new_user_mention', 'new_store_mention'
        ]
        // comment_request, buy_request, new_chat 'new_order', 'cancel_order',
    },
    loc: {
      $near: [
        lon,
        lat
      ],
      $maxDistance: maxDistance
    }
  }

  var sortedOrder = (sort === 'desc') ? -1 : 1;
  var query_options = {
    page: page,
    limit: perPage,
    columns: '_id aid actor verb product message target ref published published_timestamp loc',
    sortBy: {
      published_timestamp: sortedOrder
    }
  }

  // return new Promise(function(resolve, reject) {
  //   Activities.find(query).exec(function(err, activities) {
  //     if (err) {
  //       reject(err)
  //     } else {
  //       console.log('# of nearby users:' + lat + ' ' + lon);
  //       console.log(activities.length);
  //       // var activities = _.map(activities, function(a) {
  //       //   return location.uid;
  //       // });
  //       resolve(activities);
  //     }
  //   })
  // }).catch(function(err) {
  //   console.log(err.stack);
  //   return Promise.resolve([]);
  // });



  var output = {};

  return exports.activitiesPaginateAsync(q, query_options, options).then(function(rawData) {

    var totalItemCount = rawData.totalItemCount;
    var totalPageCount = rawData.totalPageCount;
    var activities = rawData.data;

    activities = _.map(activities, function(activity) {
      // console.log(activity);// var result = activity;
      activity.ui_message = exports.constructUIMessage(options.uid, activity, options);
      activity.card_size = 'small';
      return activity;
    });

    activities = _.filter(activities, function(o) {
      return o.is_active;
    });

    var d = new Date();
    output = {
      server_timestamp: d.getTime(),
      cards: activities
    };
    output.total = totalItemCount;
    output.pages = totalPageCount;
    output.page = page;
    output.per_page = perPage;

    if (totalItemCount <= 0) {
      output.has_more = false;
    } else {
      output.has_more = (totalPageCount > page) ? true : false;
    }

    return exports.getAndUpdateLastQueryTimestamp(options.uid);
  }).then(function(lastData) {
    //console.log(lastData);
    if (lastData) {
      output.last_query_timestamp = lastData.last_query_timestamp.getTime();
    } else {
      output.last_query_timestamp = null;
    }

    return Promise.resolve(output);
  }).catch(function(e) {
    console.log(e.stack);
    return Promise.reject(e);
  });
}


exports.getNearbyMoments = function(options) {

  var lat = options.lat;
  var lon = options.lon;

  // if (!options.lat || !options.lon) {
  //   console.log('# of nearby moment:' + options.lat + ' ' + options.lat);
  //   console.log(0);
  //   return Promise.resolve([]);
  // }

  var maxDistance = NEARBY_DIST || 40000;
  console.log('distance range in km: ' + maxDistance);
  maxDistance /= 111.12;

  var sort = options.sort || 'desc';
  sort = (_.indexOf(['desc', 'asc'], sort) >= 0) ? sort : 'desc';
  // var perPage = (perPage <= 0) ? config.activities.defaultPerPage : 10;
  // perPage = (perPage > config.activities.maxPerPage) ? config.activities.maxPerPage : perPage;

  var perPage = parseInt(options.per_page) || 0;
  perPage = (perPage <= 0) ? config.activities.defaultPerPage : perPage;
  perPage = (perPage > config.activities.maxPerPage) ? config.activities.maxPerPage : perPage;

  var page = parseInt(options.page, 10) || 1;

  var q = {
    "is_active": true,
    // "actor.id": { $ne: options.uid },
    // "target.id": options.uid,
    verb: {
      $in: ['new_moment', 'system_notification_announcement', 'new_product', 'new_newsevent'
          //      'comment_moment', 'comment_service', 'new_following', 'buy_product', 'buy_serivce', 'disapprove_product', 'store_review', 'like_service', 'like_product', 'like_moment', 'new_user_mention', 'new_store_mention'
        ]
        // comment_request, buy_request, new_chat 'new_order', 'cancel_order',
    }
  }


  var sortedOrder = (sort === 'desc') ? -1 : 1;
  var query_options = {
    page: page,
    limit: perPage,
    columns: '_id aid actor verb product message target ref published published_timestamp loc',
    sortBy: {
      published_timestamp: sortedOrder
    }
  }

  // return new Promise(function(resolve, reject) {
  //   Activities.find(query).exec(function(err, activities) {
  //     if (err) {
  //       reject(err)
  //     } else {
  //       console.log('# of nearby users:' + lat + ' ' + lon);
  //       console.log(activities.length);
  //       // var activities = _.map(activities, function(a) {
  //       //   return location.uid;
  //       // });
  //       resolve(activities);
  //     }
  //   })
  // }).catch(function(err) {
  //   console.log(err.stack);
  //   return Promise.resolve([]);
  // });



  var output = {};

  return exports.activitiesPaginateAsync(q, query_options, options).then(function(rawData) {

    var totalItemCount = rawData.totalItemCount;
    var totalPageCount = rawData.totalPageCount;
    var activities = rawData.data;

    activities = _.map(activities, function(activity) {
      // console.log(activity);// var result = activity;
      activity.ui_message = exports.constructUIMessage(options.uid, activity, options);
      activity.card_size = 'big';
      return activity;
    });

    activities = _.filter(activities, function(o) {
      return o.is_active;
    });

    var d = new Date();
    output = {
      server_timestamp: d.getTime(),
      cards: activities
    };
    output.total = totalItemCount;
    output.pages = totalPageCount;
    output.page = page;
    output.per_page = perPage;

    if (totalItemCount <= 0) {
      output.has_more = false;
    } else {
      output.has_more = (totalPageCount > page) ? true : false;
    }

    return exports.getAndUpdateLastQueryTimestamp(options.uid);
  }).then(function(lastData) {
    //console.log(lastData);
    if (lastData) {
      output.last_query_timestamp = lastData.last_query_timestamp.getTime();
    } else {
      output.last_query_timestamp = null;
    }

    return Promise.resolve(output);
  }).catch(function(e) {
    console.log(e.stack);
    return Promise.reject(e);
  });
}


exports.getNearbyUser = function(lat, lon) {

  if (!lat || !lon) {
    console.log('# of nearby users:' + lat + ' ' + lon);
    console.log(0);
    return Promise.resolve([]);
  }


  var query = {};
  var maxDistance = NEARBY_DIST || 40000;
  console.log('distance range in km: ' + maxDistance);
  maxDistance /= 111.12;
  query.loc = {
    $near: [
      lon,
      lat
    ],
    $maxDistance: maxDistance
  }

  return new Promise(function(resolve, reject) {
    Location.find(query).exec(function(err, locations) {
      if (err) {
        reject(err)
      } else {
        console.log('# of nearby users:' + lat + ' ' + lon);
        console.log(locations.length);
        var users = _.map(locations, function(location) {
          return location.uid;
        });
        resolve(users);
      }
    })
  }).catch(function(err) {
    console.log(err.stack);
    return Promise.resolve([]);
  });
}

exports.getAllActiveUser = function() {
  return models.UserR.findAll({
    where: {
      status: {
        $ne: 'inactive'
      }
    }
  }).then(function(all) {
    var users = [];
    users = _.map(all, function(user) {
      return user.id;
    });
    console.log('total # of active users ' + users.length);
    //console.log(users);
    return Promise.resolve(users);
  }).catch(function(err) {
    console.log(err.stack);
    return Promise.resolve([]);
  });
}

exports.suggestUsername = function(name) {
  var username = '@' + name.toLowerCase().trim().split(' ').join('_');
  var temp = username;

  var promiseWhile = function(condition, action) {
    var resolver = Promise.defer();
    var loop = function() {
      if (!condition()) return resolver.resolve();
      return Promise.cast(action())
        .then(loop)
        .catch(resolver.reject);
    };

    process.nextTick(loop);
    return resolver.promise;
  };

  var suffix = '';
  var user = 1;
  return promiseWhile(function() {
    return user
  }, function() {
    // Action to run, should return a promise
    return new Promise(function(resolve, reject) {

      temp = username + suffix;
      models.UserR.findOne({
        where: {
          username: temp
        }
      }).then(function(result) {
        user = result;

        if (suffix == '') {
          suffix = 1;
        } else {
          suffix += 1;
        }

        resolve();
      });
    });
  }).then(function() {
    // Notice we can chain it because it's a Promise, 
    // this will run after completion of the promiseWhile Promise!
    console.log(temp);
    return Promise.resolve(temp);
  });
}


exports.suggestStorehandle = function(name) {
  var username = '$' + name.toLowerCase().trim().replace(' ', '_');
  var temp = username;

  var promiseWhile = function(condition, action) {
    var resolver = Promise.defer();
    var loop = function() {
      if (!condition()) return resolver.resolve();
      return Promise.cast(action())
        .then(loop)
        .catch(resolver.reject);
    };

    process.nextTick(loop);
    return resolver.promise;
  };

  var suffix = 0;
  var user = 1;
  promiseWhile(function() {
    return user
  }, function() {
    // Action to run, should return a promise
    return new Promise(function(resolve, reject) {

      suffix += 1;
      temp = username + suffix;
      models.StoreR.findOne({
        where: {
          handle: temp
        }
      }).then(function(result) {
        user = result;
        resolve();
      });
    });
  }).then(function() {
    // Notice we can chain it because it's a Promise, 
    // this will run after completion of the promiseWhile Promise!
    console.log(temp);
    return Promise.resolve(temp);
  });
}

exports.updateActivity = function(option) {
  console.log('updating activity:');
  console.log(option);
  var result = {};
  new Promise(function(resolve, reject) {
    if (option.product_id) {
      delete option.moment_id;
      delete option.request_id;
      return exports.getFeedModel(option).then(function(data) {
        result.product = data;
        result.target_json = data;
        result.card_size = data.card_size || 'big';
        result.card_type = data.card_type;
        result.is_active = data.is_active || false;
        delete result.target_json.card_type;
        delete result.target_json.card_size;
        resolve(result);
      })
    } else if (option.moment_id) {
      delete option.product_id;
      delete option.request_id;
      return exports.getFeedModel(option).then(function(data) {
        result.moment = data;
        result.target_json = data;
        result.card_size = data.card_size || 'big';
        result.card_type = data.card_type;
        result.is_active = data.is_active || false;
        delete result.target_json.card_type;
        delete result.target_json.card_size;

        resolve(result);
      });
    } else if (option.request_id) {
      delete option.product_id;
      delete option.moment_id;
      return exports.getFeedModel(option).then(function(data) {
        result.request = data;
        result.target_json = data;
        result.card_size = data.card_size || 'big';
        result.card_type = data.card_type;
        result.is_active = data.is_active || false;
        delete result.target_json.card_type;
        delete result.target_json.card_size;
        return Promise.resolve(result);
      });
    } else if (option.newsevent_id) {
      delete option.product_id;
      delete option.moment_id;
      delete option.request_id;

      return exports.getFeedModel(option).then(function(data) {
        result.request = data;
        result.target_json = data;
        result.card_size = data.card_size || 'big';
        result.card_type = data.card_type;
        result.is_active = data.is_active || false;
        delete result.target_json.card_type;
        delete result.target_json.card_size;
        return Promise.resolve(result);
      });
    } else {
      resolve({});
    }
  }).then(function(data) {
    //console.log('updating activity: ' + JSON.stringify(data.target_json));
    var result = data.target_json;
    if (result && result.type && result.id) {
      var option1 = {};
      //option1[result.type + '.id'] = result.id;
      //console.log(data.moment);
      // Activities.update(option1, _.pick(data, ['is_active', 'product', 'service', 'request', 'moment', 'target_json']), { multi: true }, function(err, num) {
      //   if (err) {
      //     console.log(err.stack);
      //   } else {
      //     console.log("updated " + num + " activities of " + result.type + " " + result.id);
      //   }
      // option1 = {
      //   $or: [{
      //     result.type + '.id': result.id
      //   }, { result.type + '.id': result.id + '' }]
      // };

      option1['$or'] = [{}, {}];
      option1['$or'][0][result.type + '.id'] = result.id;
      option1['$or'][1][result.type + '.id'] = result.id + '';

      // just in case service object in product object
      if (result.type == 'service') {
        option1['$or'].push({});
        option1['$or'][2]['product.id'] = result.id;
        option1['$or'].push({});
        option1['$or'][3]['product.id'] = result.id + '';
      }

      option1['$or'] = _.without(option1['$or'], {});
      //console.log(option1);


      Activities.findAsync(option1).then(function(activities) {
        //console.log(activities.length + " activities of " + result.type + " " + result.id);
        _.map(activities, function(activity) {

          // _.merge(activity, _.pick(data, ['is_active', 'product', 'service', 'request', 'moment', 'target_json']));
          //console.log(data.target_json);
          activity.target_json = data.target_json;
          activity.product = data.product;
          activity.service = data.service;
          activity.moment = data.moment;
          activity.is_active = data.is_active;
          //console.log(activity);
          activity.save();
          exports.updateSocialActivity(activity, data);
        });
      });

      // });
    }
  }).catch(function(err) {
    console.log(err.stack);
    return Promise.resolve({});
  });
}

exports.updateSocialActivity = function(activity, data) {

  SocialActivities.update({ activity_id: activity.aid }, _.pick(data, ['is_active']), { multi: true }, function(err, num) {
    if (err) {
      console.log(err.stack);
    } else {
      console.log("updated " + num + " socialactivities related to activity_id " + activity.aid);
    }
  });
}

// exports.updateActivity = function(model) {
//   var is_active = model.is_enabled && !model.is_deleted && model.is_visible && model.approved;

//   var type = model.type;

//   var option = {};
//   option[type + '.id'] = model.id;

//   Activities.update(option, { is_active: is_active }, { multi: true }, function(err, num) {
//     if (err) {
//       console.log(err.stack);
//     } else {
//       console.log("updated " + num + " activities of" + type + " " + model.id);
//     }
//   });
// }


exports.update_store = function(id) {

  var vancouver_loc = {
    lat: 49.2822321,
    lon: -123.1248933,
    address1: '909 Burrard St',
    address2: 'Vancouver'

  }

  var seattle_loc = {
    lat: 47.6205063,
    lon: -122.3492774,
    address1: '5th Ave N & Broad St',
    address2: 'Seattle'
  }

  var filterPictureUrl = function(images) {
    var array = _.map(images, function(image) {
      if (image.indexOf('vignette.png') > -1) {
        var myString = image.substring(0, image.indexOf('?'))
        console.log(myString);
        return myString;
      } else {
        return image;
      }
    })
    return array;
  };


  var store = null;
  return models.Store
    .findById(id)
    .then(function(model) {
      if (!model) {
        return Promise.reject(null);
      }
      store = model;
      var storeID = id;
      var attrs = {};
      if (!store.lat && !store.lon && !store.address2 && !store.address1) {
        if (store.country == 'US') {
          attrs = seattle_loc;
        } else {
          attrs = vancouver_loc;
        }
      }

      // if (!store.logo) {
      //   attrs.logo = 'https://antsquare.imgix.net/antsquare/default_shop.jpg'
      // }

      console.log('updateStoreProductCount called with storeID: ' + storeID);
      var uniqueProductsInStore = 0;
      var store_category = 'Other Products';
      var total_review = 0;
      var average_rating = 0;

      return rp(config.file + '/shopavatar?name=' + store.store_name).then(function(url_info) {
        if (url_info) {
          var url = JSON.parse(url_info).url;
          console.log(url);
          if (!store.logo || store.logo.indexOf('default_shop.jpg') > -1 || store.logo.indexOf('avatar') > -1) {
            attrs.logo = url;
          }
        } else {
          if (!store.logo) {
            attrs.logo = 'https://antsquare.imgix.net/antsquare/default_shop.jpg'
          }
        }

        return models.Comment.findAll({
          where: {
            store_id: id,
            type: "review"
          }
        });
      }).then(function(reviews) {
        console.log("review count:" + reviews.length)
        total_review = reviews.length;
        var reviewSum = 0;
        _.map(reviews, function(review) {
          reviewSum = reviewSum + review.star;
        });
        if (reviews.length > 0) {
          average_rating = reviewSum / reviews.length;
          console.log('average review: ' + average_rating)
        }

        return models.Product.findAll({
          where: {
            store_id: id
          },
          limit: 1,
          order: 'id DESC'
        });
      }).then(function(products) {

        if (products.length > 0) {
          store_category = products[0].category;
        }

        return models.Product.count({
          where: {
            store_id: storeID,
            is_visible: true,
            approved: true,
            is_deleted: false,
            is_enabled: true
          }
        })
      }).then(function(uniqueProducts) {
        console.log(uniqueProducts);
        uniqueProductsInStore = uniqueProducts;
        return models.Product.sum('quantity', {
          where: {
            store_id: storeID,
            is_visible: true,
            is_deleted: false,
            approved: true,
            is_enabled: true
          }
        })
      }).then(function(sum) {
        console.log(sum);
        if (!sum) {
          sum = 0;
        }
        return store.updateAttributes(_.merge(attrs, {
          total_product: sum,
          unique_product: uniqueProductsInStore,
          as_business: true,
          store_category: store_category,
          total_review: total_review,
          average_rating: average_rating,
          resource_id: 'shop_' + store.id
        }));
      });

    }).then(function(model) {

      console.log('updating for index of store ' + model.id);

      // intercom_client.createUser({
      //   user_id: model.user_id + "",
      //   custom_data: {
      //     store: config.dashboard + '/store/' + store.id,
      //     store_name: store.name
      //   }
      // }, function(err, r) {
      //   if (err)
      //     console.log(err);
      // });

      return models.Product.findAll({
        where: {
          store_id: id
        }
      }).then(function(storeProducts) {
        var aggregateStoreProductModel = {
            store: model.toElasticSearchModel(),
            product: storeProducts
          }
          //console.log("target data: " + JSON.stringify(aggregateStoreProductModel))
        return Promise.resolve(aggregateStoreProductModel)
      });
    })
    .then(function(storeProducts) {
      //console.log("target data: " + JSON.stringify(storeProducts))
      var all = _.map(storeProducts.product, function(model, index) {
        var attrs = {};
        attrs.currency = (store.country == 'CA' ? 'cad' : 'usd');
        attrs.lat = store.lat;
        attrs.lon = store.lon;
        attrs.address1 = store.address1;
        attrs.address2 = store.address2;
        attrs.user_id = store.user_id;
        attrs.is_enabled = store.is_enabled;
        attrs.is_visible = store.is_enabled;
        attrs.is_pretty = store.is_pretty;
        attrs.resource_id = 'prod_' + model.id;
        //console.log("broken model: " + JSON.stringify(model))
        if (model.quantity <= 0) {
          attrs.quantity = 1;
        }

        if (Number(model.price) <= 0) {
          attrs.price = 0;
        }

        if (!model.images || model.images.length < 1) {
          attrs.has_image = false;
        } else {
          attrs.has_image = true;
        }

        attrs.images = filterPictureUrl(model.images);

        switch (model.category) {

          case 'accessories':
            attrs.category = 'Fashion & Accessories';
            break;
          case 'food':
            attrs.category = 'Food';
            break;
          case 'antiques':
            attrs.category = 'Arts & Crafts';
            break;
          case 'art, collectibles':
            attrs.category = 'Arts & Crafts';
            break;
          case 'auto parts':
            attrs.category = 'Other Products';
            break;
          case 'baby items':
            attrs.category = 'Baby & Kids';
            break;
          case 'bikes':
            attrs.category = 'Other Products';
            break;
          case 'bike parts & accessories':
            attrs.category = 'Other Products';
            break;
          case 'books':
            attrs.category = 'Used';
            break;
          case 'office, stationary':
            attrs.category = 'Home & Living';
            break;
          case 'cameras, camcorders':
            attrs.category = 'Electronics';
            break;
          case 'cds, dvds, blu-ray':
            attrs.category = 'Electronics';
            break;
          case 'clothing':
            attrs.category = 'Fashion & Accessories';
            break;
          case 'computers':
            attrs.category = 'Electronics';
            break;
          case 'computer accessories':
            attrs.category = 'Electronics';
            break;
          case 'electronics':
            attrs.category = 'Electronics';
            break;
          case 'furniture':
            attrs.category = 'Home & Living';
            break;
          case 'totes, handbags, backpacks':
            attrs.category = 'Fashion & Accessories';
            break;
          case 'health, special needs':
            attrs.category = 'Sports & Recreation';
            break;
          case 'hobbies, crafts':
            attrs.category = 'Sports & Recreation';
            break;
          case 'home appliances':
            attrs.category = 'Home & Living';
            break;
          case 'home - indoor':
            attrs.category = 'Home & Living';
            break;
          case 'home - outdoor':
            attrs.category = 'Home & Living';
            break;
          case 'home renovation materials':
            attrs.category = 'Home & Living';
            break;
          case 'iPods, mp3, headphones':
            attrs.category = 'Electronics';
            break;
          case 'jewelry':
            attrs.category = 'Jewelry';
            break;
          case 'musical instruments':
            attrs.category = 'Sports & Recreation';
            break;
          case 'phones':
            attrs.category = 'Electronics';
            break;
          case 'sporting goods, exercise':
            attrs.category = 'Sports & Recreation';
            break;
          case 'tickets':
            attrs.category = 'Sports & Recreation';
            break;
          case 'tools':
            attrs.category = 'Home & Living';
            break;
          case 'toys, games':
            attrs.category = 'Sports & Recreation';
            break;
          case 'video games, consoles':
            attrs.category = 'Sports & Recreation';
            break;
          case 'watches':
            attrs.category = 'Jewelry';
            break;
          case 'other products':
            attrs.category = 'Other Products';
            break;
          case 'barbers & hair services':
            attrs.category = 'Hair & Barber';
            break;
          case 'childcare, nanny':
            attrs.category = 'Other Service';
            break;
          case 'cleaners, cleaning':
            attrs.category = 'Cleaning & Landscaping';
            break;
          case 'computer repair':
            attrs.category = 'Maintenance & Repair';
            break;
          case 'IT service':
            attrs.category = 'Design & Tech';
            break;
          case 'entertainment':
            attrs.category = 'Entertainment';
            break;
          case 'financial, legal':
            attrs.category = 'Other Service';
            break;
          case 'fitness, personal trainer':
            attrs.category = 'Fitness';
            break;
          case 'health, beauty':
            attrs.category = 'Beauty & Spa';
            break;
          case 'moving, storage':
            attrs.category = 'Delivery';
            break;
          case 'music lessons':
            attrs.category = 'Lessons';
            break;
          case 'phone repair':
            attrs.category = 'Maintenance & Repair';
            break;
          case 'photography, video':
            attrs.category = 'Photography & Events';
            break;
          case 'skilled trades':
            attrs.category = 'Other Service';
            break;
          case 'travel, vacations':
            attrs.category = 'Other Service';
            break;
          case 'tutors, languages':
            attrs.category = 'Lessons';
            break;
          case 'wedding':
            attrs.category = 'Photography & Events';
            break;
          case 'other services':
            attrs.category = 'Other Service';
            break;
          default:
            ;
        }

        //attrs.type = category.findCategoryByNameV4(attrs.category);
        exports.updateActivity({ product_id: model.id });
        return model.updateAttributes(attrs);
      });
      return Promise.all(all);
    })
    .catch(models.Sequelize.ValidationError, function(err) {
      console.log(err);
      console.log(err.stack);
    })
    .catch(function(err) {
      console.log(err);
      console.log(err.stack);
    });
};

exports.update_user = function(id) {

  var vancouver_loc = {
    lat: 49.2822321,
    lon: -123.1248933,
    address1: '909 Burrard St',
    address2: 'Vancouver'

  }

  var seattle_loc = {
    lat: 47.6205063,
    lon: -122.3492774,
    address1: '5th Ave N & Broad St',
    address2: 'Seattle'
  }

  var filterPictureUrl = function(images) {
    var array = _.map(images, function(image) {
      if (image.indexOf('vignette.png') > -1) {
        var myString = image.substring(0, image.indexOf('?'))
        console.log(myString);
        return myString;
      } else {
        return image;
      }
    })
    return array;
  };

  var attrs = {};
  var user = null;
  return models.User.findById(id)
    .then(function(model) {
      if (!model) {
        return Promise.reject(null);
      }
      user = model;
      if (!user.lat && !user.lon && !user.address2 && !user.address1) {
        if (user.country == 'US') {
          attrs = seattle_loc;
        } else {
          attrs = vancouver_loc;
        }
      }
      return rp(config.file + '/initialavatar?name=' + user.name);
    }).then(function(url_info) {
      if (url_info) {
        var url = JSON.parse(url_info).url;
        console.log(url);
        if (!user.picture || user.picture.indexOf('as-avatar.jpg') > -1) {
          attrs.picture = url;
        }
      } else {
        if (!user.picture) {
          attrs.picture = 'https://antsquare.imgix.net/antsquare/as-avatar.jpg'
        }
      }
      return user.updateAttributes(attrs);
    }).then(function(model) {

      console.log('updating for index of user ' + model.id);
      user = model;
      return models.Note.findAll({
        where: {
          user_id: id
        }
      });
    }).then(function(moments) {

      //console.log("target data: " + JSON.stringify(storeProducts))
      var all = _.map(moments, function(model, index) {
        var attrs = {};
        attrs.is_active = user.is_active && model.is_active;
        attrs.resource_id = 'mmnt_' + model.id;
        attrs.quantity = 1;
        attrs.price = 0;

        if (!model.images || model.images.length < 1) {
          attrs.has_image = false;
        } else {
          attrs.has_image = true;
        }

        attrs.images = filterPictureUrl(model.images);
        exports.updateActivity({ moment_id: model.id });
        return model.updateAttributes(attrs);
      });
      return Promise.all(all);
    })
    .catch(models.Sequelize.ValidationError, function(err) {
      console.log(err);
      console.log(err.stack);
    })
    .catch(function(err) {
      console.log(err);
      console.log(err.stack);
    });
};

exports.generateRandomPass = function(plength) {

  var keylistalpha = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
  var keylistint = "123456789";
  var keylistspec = "!@#_";
  var temp = '';
  var len = plength / 2;
  var len = len - 1;
  var lenspec = plength - len - len;

  for (var i = 0; i < len; i++)
    temp += keylistalpha.charAt(Math.floor(Math.random() * keylistalpha.length));

  for (var i = 0; i < lenspec; i++)
    temp += keylistspec.charAt(Math.floor(Math.random() * keylistspec.length));

  for (var i = 0; i < len; i++)
    temp += keylistint.charAt(Math.floor(Math.random() * keylistint.length));

  temp = temp.split('').sort(function() {
    return 0.5 - Math.random()
  }).join('');

  return temp;
}

exports.updateMomentLikesCount = function(user_id) {
  console.log('updating moment like count of user ' + user_id);
  var total_moment_likes = 0;
  var self = {};
  return models.User.findById(user_id).then(function(user) {
    if (user) {
      self.user = user;
    } else {
      return Promise.reject({
        message: "User not found: " + user_id
      });
    }
  }).then(function() {
    return models.Note.sum('total_likes', {
      where: {
        user_id: user_id,
        is_visible: true,
        approved: true,
        is_deleted: false,
        is_enabled: true
      }
    });
  }).then(function(total) {
    if (total) {
      total_moment_likes = total;
    }

    return models.Product.sum('total_likes', {
      where: {
        user_id: user_id,
        is_visible: true,
        approved: true,
        is_deleted: false,
        is_enabled: true
      }
    });
  }).then(function(total) {
    if (total) {
      total_moment_likes += total;
    }

    models.Store.update({ total_moment_likes: total_moment_likes }, {
      fields: ['total_moment_likes'],
      where: {
        user_id: user_id,
      }
    });

    return self.user.updateAttributes({ total_moment_likes: total_moment_likes });
  }).then(function(user) {
    return Promise.resolve(user.toElasticSearchModel());
  }).catch(function(e) {
    console.log(e);
    return Promise.resolve({});
  });
}

exports.updateEventLikesCount = function(user_id) {}

exports.abbreviateNumber = function(value) {
  var newValue = value;
  if (value >= 10000) {
    newValue = exports.nFormatter(value, 1);
  } else if (value >= 1000) {
    //return Utils.nFormatter(value, 1);
    newValue = value.toLocaleString();
  } else {
    newValue = value + '';
  }
  return newValue;
}

exports.nFormatter = function(num, digits) {
  var si = [
      { value: 1E18, symbol: "E" },
      { value: 1E15, symbol: "P" },
      { value: 1E12, symbol: "T" },
      { value: 1E9, symbol: "G" },
      { value: 1E6, symbol: "M" },
      { value: 1E3, symbol: "k" }
    ],
    rx = /\.0+$|(\.[0-9]*[1-9])0+$/,
    i;
  for (i = 0; i < si.length; i++) {
    if (num >= si[i].value) {
      return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
    }
  }
  return num.toFixed(digits).replace(rx, "$1");
}

exports.loadMentions = function(text, actor_id, option) {
  ///^[A-Za-z0-9_]{1,15}$/
  var usernames = text.match(/@[A-Za-z0-9_]{1,15}:[0-9]{1,15}/g);
  usernames = _.uniq(usernames);
  console.log(usernames);
  var activities1 = _.map(usernames, function(username) {
    if (exports.isUsername(username)) {
      return models.UserR.findOne({ where: { username: username } }).then(function(user) {
        if (user) {
          return Promise.resolve(_.merge({
            actor_id: actor_id,
            verb: 'new_user_mention',
            target_id: user.id
          }, option));
        } else {
          return Promise.resolve({});
        }
      });
    } else {
      return Promise.resolve({});
    }
  });

  var handles = text.match(/\$[A-Za-z0-9_]{1,15}/g);
  //handles = _.uniq(handles);
  console.log(handles);
  var activities2 = _.map(handles, function(handle) {
    if (exports.isStoreHandle(handle)) {
      return models.StoreR.findOne({ where: { handle: handle } }).then(function(store) {
        if (store) {
          return Promise.resolve(_.merge({
            actor_id: actor_id,
            verb: 'new_store_mention',
            target_id: store.user_id
          }, option));
        } else {
          return Promise.resolve({});
        }
      });
    } else {
      return Promise.resolve({});
    }
  });


  var all = activities1.concat(activities2);
  Promise.all(all).then(function(activities) {
    Queue.fireActvityEvents(activities);
  }).catch(function(err) {
    console.log(err.stack);
  });
}

exports.createStoreFromYelp = function(yelp_data) {

  if (yelp_data.id) {
    return models.findOne({ where: { external_id: yelp_data.id, external_source: 'yelp' } }).then(function(store) {
      if (!store) {
        var data = {};
        data.store_name = yelp_data.name;
        data.external_source = 'yelp';
        data.external_id = yelp_data.id;
        data.store_name = yelp_data.name;
        data.claimed = false;
        data.lat = yelp_data.coordinates.latitude;
        data.lon = yelp_data.coordinates.longitude;
        data.address1 = yelp_data.location.address1;
        data.address2 = yelp_data.location.address2;
        data.state = yelp_data.location.state;
        data.country = yelp_data.location.country;
        data.logo = yelp_data.image_url;
        data.average_rating = yelp_data.rating;
        data.description = '';
        data.user_id = 0;
        return models.create(data);
      }
    }).then(function(store) {
      console.log(store.toElasticSearchModel());
    }).catch(function(err) {
      console.log(err.stack);
    })
  }
}

exports.createStoreFromGoogle = function(google_data) {

  if (google_data.id) {
    return models.findOne({ where: { external_id: google_data.id, external_source: 'google' } }).then(function(store) {
      if (!store) {
        var data = {};
        data.store_name = google_data.name;
        data.external_source = 'yelp';
        data.external_id = google_data.id;
        data.store_name = google_data.name;
        data.claimed = false;
        data.lat = google_data.coordinates.latitude;
        data.lon = google_data.coordinates.longitude;
        data.address1 = google_data.location.address1;
        data.address2 = google_data.location.address2;
        data.state = google_data.location.state;
        data.country = google_data.location.country;
        data.logo = google_data.image_url;
        data.average_rating = google_data.rating;
        data.description = '';
        data.user_id = 0;
        data.external_url = yelp_data.url;
        data.business_phone = yelp_data.phone;
        return models.create(data);
      }
    }).then(function(store) {
      console.log(store.toElasticSearchModel());
    }).catch(function(err) {
      console.log(err.stack);
    })
  }
}

// exports.createActivity = function(uid, body) {

//   User.findOneAsync({ uid: uid }).then(function(user) {
//     if (!user) {
//       return;
//     }
//     var jwt = user.generateJwt1();

//     var option1 = {
//       uri: config.feed + '/v2/activities',
//       method: 'POST',
//       json: true,
//       body: body,
//       headers: {
//         'authorization': 'Bearer ' + jwt
//       }
//     };
//     //console.log(option1);
//     rp(option1).then(function(data) {
//       //console.log(data);
//       console.log(body);
//     }).catch(function(err) {
//       console.log(err);
//     });

//   });

// }


exports.getNearbyParking = function(lat, lon) {

  var url = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=' + lat + ',' + lon + '&radius=1000&type=parking&key=' + config.google.api_key;
  console.log(url);

  return rp(url).then(function(response) {
    //console.log(response);
    var address = {};
    var response = JSON.parse(response);
    var parking = []
    var min = Math.min(5, response.results.length);

    console.log('length: ' + min);
    var results = response.results;

    for (var i = 0; i < min; i++) {

      parking.push({ location: { lat: results[i].geometry.location.lat, lon: results[i].geometry.location.lon }, id: results[i].id, name: results[i].name });
    }

    console.log(parking);
    return Promise.resolve(parking);
  });
}